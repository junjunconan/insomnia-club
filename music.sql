use insomnia;

drop table if exists musicrating;
drop table if exists musiccomment;
drop table if exists music;


CREATE table music(
	id	int(50)	not null primary key auto_increment,
	musicname varchar(50) not null ,
	musictype varchar(50),
	singer varchar(50),
	keyword1 varchar(50),
	keyword2 varchar(50),
	keyword3 varchar(50),
    email varchar(255),
    filename varchar(255),
	foreign key (email) references user(email) on delete cascade on update cascade
);

CREATE table musicrating(
	id	int(50)	not null primary key auto_increment,
	rating	int(50)	not null,
	user_id	varchar(255),
	music_id int(50),
	foreign key (user_id)	references user(email) on delete cascade on update cascade,
	foreign key (music_id)	references music(id) on delete cascade on update cascade
);

CREATE table musiccomment(
	id	int(50) not null primary key auto_increment,
	comment	text	not null,
	user_id	varchar(255),
	Music_id	int(50),
	foreign key(user_id) references user(email) on delete cascade on update cascade,
	foreign key(music_id)	references music(id) on delete cascade on update cascade
);