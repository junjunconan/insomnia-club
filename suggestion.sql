create database if not exists insomnia;

use insomnia;

drop table if exists suggestionrating;
drop table if exists suggestioncomment;
drop table if exists suggestion;

create table suggestion (
  id			int(11)		not null primary key auto_increment,
  title			varchar(255) not null,
  description	text		not null,
  user_id		varchar(255),
  date		    timestamp,
  foreign key (user_id)		references user(email) on delete cascade on update cascade
);

create table suggestionrating (
  id			int(11)		not null primary key auto_increment,
  rating		int(11)		not null,
  user_id		varchar(255),
  suggestion_id int(11),
  date		    timestamp,
  foreign key (user_id) 	references 		user(email) on delete cascade on update cascade,
  foreign key (suggestion_id) references 	suggestion(id) on delete cascade on update cascade
);

create table suggestioncomment (
  id			int(11)		not null primary key auto_increment,
  comment		text		not null,
  user_id		varchar(255),
  suggestion_id int(11),
  date		    timestamp,
  foreign key (user_id) 	references 		user(email) on delete cascade on update cascade,
  foreign key (suggestion_id) references 	suggestion(id) on delete cascade on update cascade
);