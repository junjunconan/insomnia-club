package insomniaclub.video.service;



import insomniaclub.video.model.videotag;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;

@Transactional
public class VideoTagDao implements IVideoTagDao{
	
	@Autowired
	private HibernateTemplate hibernateTemplate;
	
	@Override
	public void saveVideoTag(videotag videoTag) {
		hibernateTemplate.save(videoTag);
	}

	@Override
	public void updateVideoTag(videotag videoTag) {
		hibernateTemplate.update(videoTag);
	}

	@Override
	public void deleteVideoTag(videotag videoTag) {
		hibernateTemplate.delete(videoTag);
	}

	@Override
	public List<videotag> findAll() {
		return hibernateTemplate.loadAll(videotag.class);
	}
	
}