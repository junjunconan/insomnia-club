package insomniaclub.video.service;

import java.util.List;

import insomniaclub.authentication.model.User;
import insomniaclub.video.model.Video;

public interface IVideoDao {

	public void saveVideo(Video video);

	public void deleteVideo(Video video);
	
	public Video findByVideoId(int videoid);
	
	public List<Video> findByCate(String cate);

	List<Video> findAll();

	public List<Video> orderFind();

}