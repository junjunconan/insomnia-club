package insomniaclub.video.service;

import java.util.List;

import insomniaclub.authentication.model.User;
import insomniaclub.video.model.Video;
import insomniaclub.video.model.VideoRate;

public interface IVideoRateDao {

	public void saveVideoRate(VideoRate videoRate);

	public void updateVideoRate(VideoRate videoRate);

	public void deleteVideoRate(VideoRate videoRate);

	public List<VideoRate> findAll();

	public List<VideoRate> findByVideoUser(Video video,
			User user);

	List orderByRate();


}
