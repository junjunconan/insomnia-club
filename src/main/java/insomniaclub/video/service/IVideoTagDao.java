package insomniaclub.video.service;

import java.util.List;

import insomniaclub.video.model.videotag;

public interface IVideoTagDao{
	public void saveVideoTag(videotag videoTag);
	
	public void deleteVideoTag(videotag videoTag);
	
	public void updateVideoTag(videotag videoTag);
	
	public List<videotag> findAll();
}