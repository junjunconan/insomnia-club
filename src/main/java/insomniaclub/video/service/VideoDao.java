package insomniaclub.video.service;

import java.util.List;

import insomniaclub.authentication.model.User;
import insomniaclub.suggestion.model.Suggestion;
import insomniaclub.timetable.model.TimeTable;
import insomniaclub.video.model.Video;

import javax.transaction.Transactional;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;


@Transactional
public class VideoDao implements IVideoDao {

	@Autowired
	private HibernateTemplate hibernateTemplate;

	@Override
	public void saveVideo(Video video){
		hibernateTemplate.save(video);
	}

	@Override
	public void deleteVideo(Video video){
		hibernateTemplate.delete(video);
	}
	
	@Override
	public Video findByVideoId(int videoid) {
		return hibernateTemplate.get(Video.class, videoid);
	}
	
	
	@Override
	public List<Video> findAll() {
		return hibernateTemplate.loadAll(Video.class);
	}

	@Override
	public List<Video> findByCate(String cate) {
		return (List<Video>) hibernateTemplate.find("FROM Video v WHERE v.category=?", cate);
	}

	@Override
	public List<Video> orderFind() {
		Session session = hibernateTemplate.getSessionFactory().getCurrentSession();
		Query query = session.createQuery("FROM Video v order by rand()").setMaxResults(5);
		return query.list();
	}

}

