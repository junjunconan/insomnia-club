package insomniaclub.video.service;

import insomniaclub.authentication.model.User;
import insomniaclub.video.model.Video;
import insomniaclub.video.model.VideoRate;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;

@Transactional
public class VideoRateDao implements IVideoRateDao {

	@Autowired
	private HibernateTemplate hibernateTemplate;

	@Override
	public void saveVideoRate(VideoRate videoRate) {
		hibernateTemplate.save(videoRate);
	}

	@Override
	public void updateVideoRate(VideoRate videoRate) {
		hibernateTemplate.update(videoRate);
	}

	@Override
	public void deleteVideoRate(VideoRate videoRate) {
		hibernateTemplate.delete(videoRate);
	}

	@Override
	public List<VideoRate> findAll() {
		return hibernateTemplate.loadAll(VideoRate.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<VideoRate> findByVideoUser(Video video, User user) {
		String[] paramNames = {"video", "user"};
		Object[] paramValues = {video, user};
		return (List<VideoRate>) hibernateTemplate
				.findByNamedParam("FROM VideoRate v WHERE v.video=:video AND v.user=:user",
									paramNames, paramValues);
	}
	
	@Override
	@SuppressWarnings("rawtypes")
	public List orderByRate() {
		Session session = hibernateTemplate.getSessionFactory().getCurrentSession();
		Query query = session.createQuery("SELECT v.video, avg(v.videorate) FROM VideoRate v " +
				"GROUP BY v.video " +
				"ORDER BY avg(v.videorate) DESC").setMaxResults(5);
		return query.list();
	}

}
