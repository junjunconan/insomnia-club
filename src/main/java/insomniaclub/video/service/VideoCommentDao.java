package insomniaclub.video.service;

import insomniaclub.suggestion.model.SuggestionComment;
import insomniaclub.video.model.Video;
import insomniaclub.video.model.VideoComment;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;

@Transactional
public class VideoCommentDao implements IVideoCommentDao {

	@Autowired
	private HibernateTemplate hibernateTemplate;

	@Override
	public void saveVideoComment(VideoComment videoComment) {
		hibernateTemplate.save(videoComment);
	}

	@Override
	public void updateVideoComment(VideoComment videoComment) {
		hibernateTemplate.update(videoComment);
	}

	@Override
	public void deleteVideoComment(VideoComment videoComment) {
		hibernateTemplate.delete(videoComment);
	}

	@Override
	public List<VideoComment> findAll() {
		return hibernateTemplate.loadAll(VideoComment.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<VideoComment> findByVideo(Video video) {
		return (List<VideoComment>) hibernateTemplate
				.findByNamedParam("FROM VideoComment v WHERE v.video=:video",
									"video", video);
	}

}