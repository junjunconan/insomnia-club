package insomniaclub.video.service;

import insomniaclub.video.model.Video;
import insomniaclub.video.model.VideoComment;

import java.util.List;

public interface IVideoCommentDao {

	public void saveVideoComment(VideoComment VideoComment);

	public void updateVideoComment(VideoComment VideoComment);

	public void deleteVideoComment(VideoComment VideoComment);

	public List<VideoComment> findAll();

	public List<VideoComment> findByVideo(Video video);

}
