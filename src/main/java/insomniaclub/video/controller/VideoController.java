package insomniaclub.video.controller;

import insomniaclub.authentication.model.User;
import insomniaclub.video.model.Video;
import insomniaclub.video.model.VideoComment;
import insomniaclub.video.model.VideoRate;
import insomniaclub.video.service.IVideoCommentDao;
import insomniaclub.video.service.IVideoDao;
import insomniaclub.video.service.IVideoRateDao;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.fasterxml.jackson.databind.ObjectMapper;

@Controller
@Scope("session")
@RequestMapping("/insomniaclub")
public class VideoController{
	@Autowired
	private IVideoDao videoDao;
	@Autowired
	private IVideoCommentDao vcDao;
	@Autowired
	private IVideoRateDao vrDao;

	@RequestMapping(value = "videos/{cate}")
	public String video(HttpSession session, Model model, @PathVariable String cate){
		User user;
		if((user = (User)session.getAttribute("user")) == null)
			return "redirect:/insomniaclub";
		List<Video> videos = videoDao.findByCate(cate);
		List<String> list = new ArrayList<String>();
		for(Video v : videos){
			list.add(v.getCategory());
		}
		model.addAttribute("videos", videos);
		ObjectMapper mapper = new ObjectMapper();
		String json="";
		try {
			json = mapper.writeValueAsString(list);
		} catch (Exception e) {
			e.printStackTrace();
		}
		model.addAttribute("list", json);

		if (cate.equals("intro"))
			model.addAttribute("cate", "What is Insomnia");
		else if (cate.equals("exe"))
			model.addAttribute("cate", "Bedtime Exercise");
		else
			model.addAttribute("cate", "Overcome Insomnia");

		return "videos";
	}

	@RequestMapping(value = "videos")
	public String v(HttpSession session, Model model){
		User user;
		if((user = (User)session.getAttribute("user")) == null)
			return "redirect:/insomniaclub";
		List<Video> videos = videoDao.orderFind();
		List<String> list = new ArrayList<String>();
		for(Video v : videos){
			list.add(v.getCategory()); //the variable?
		}
		model.addAttribute("videos", videos);
		ObjectMapper mapper = new ObjectMapper();
		String json="";
		try {
			json = mapper.writeValueAsString(list);
		} catch (Exception e) {
			e.printStackTrace();
		}
		model.addAttribute("list", json);
		return "v";
	}

	@RequestMapping(value = "videos/commentandrate/{videoid}")
	public String commentAndRate(HttpSession session, Model model,
			@PathVariable int videoid) {
		User user;
		if ((user = (User) session.getAttribute("user")) == null)
			return "redirect:/insomniaclub";

		Video video = videoDao.findByVideoId(videoid);
		if (video == null)
			return "redirect:/insomniaclub";

		List<VideoComment> vcs = vcDao.findByVideo(video);
		List<CommentModel> result = new ArrayList<CommentModel>();
		List<Integer> videocommentList = new ArrayList<Integer>();
		for (VideoComment vc : vcs) {
			CommentModel commentm = new CommentModel();
			commentm.comment_id = vc.getCommentId();
			commentm.comment = vc.getComment();
			commentm.user = vc.getUser();
			commentm.videorate = vrDao.findByVideoUser(video, vc.getUser())
					.get(0).getVideoRate();
			result.add(commentm);
			videocommentList.add(vc.getCommentId());
		}

		ObjectMapper mapper = new ObjectMapper();
		String json="";
		try {
			json = mapper.writeValueAsString(videocommentList);
		} catch (Exception e) {
			e.printStackTrace();
		}
		model.addAttribute("commentlist", json);
		model.addAttribute("video", video);
		model.addAttribute("result", result);

		List<VideoRate> ratings = vrDao.findByVideoUser(video, user);
		if (ratings.size() > 0)
			model.addAttribute("rated", ratings.get(0).getVideoRate());
		else
			model.addAttribute("rated", -1);

		return "vcr";
	}

	@RequestMapping(value = "videos/commentandrate/{videoid}", method = RequestMethod.POST)
	public String postCommentAndRate(@RequestParam int videorate, @RequestParam String comment,
			HttpSession session, @PathVariable int videoid) {
		User user;
		if ((user = (User) session.getAttribute("user")) == null)
			return "redirect:/insomniaclub";

		Video video = videoDao.findByVideoId(videoid);
		if (video == null)
			return "redirect:/insomniaclub";

		List<VideoRate> videorates = vrDao.findByVideoUser(video, user);
		VideoRate videoRate = videorates.isEmpty() ?
				new VideoRate(user, video) : videorates.get(0);
		videoRate.setVideoRate(videorate);
		vrDao.saveVideoRate(videoRate);

		if (!comment.isEmpty()) {
			VideoComment videoComment = new VideoComment(comment, user, video);
			vcDao.saveVideoComment(videoComment);
		}

		return "redirect:/insomniaclub/videos/commentandrate/" + videoid;
	}

	@SuppressWarnings("unused")
	private static class CommentModel {

		public int comment_id;
		public User user;
		public int videorate;
		public String comment;

	}


}

