package insomniaclub.video.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table (name = "video")
public class Video {

	@Id
	@Column(name = "videoid")
	private int videoid;

	@Column(name = "videotitle")
	private String videotitle;

	@Column(name = "category")
	private String category;

	@Column(name = "description")
	private String description;

	@Column(name = "videopath")
	private String videopath;

	public void setVideoid(int videoid) {
		this.videoid = videoid;
	}

	public int getVideoid() {
		return videoid;
	}

	public void setVideotitle(String videotitle) {
		this.videotitle = videotitle;
	}

	public String getVideotitle() {
		return videotitle;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getCategory() {
		return category;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

	public void setVideopath(String videopath) {
		this.videopath = videopath;
	}

	public String getVideopath() {
		return videopath;
	}
}
