package insomniaclub.video.model;

import insomniaclub.authentication.model.User;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table (name = "videorate")
public class VideoRate {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "videorateid")
	private int videorateid;

	@Column(name = "videorate")
	private int videorate;

	@ManyToOne(cascade=CascadeType.PERSIST)
	@JoinColumn(name="email", referencedColumnName = "email")
	private User user;

	@ManyToOne(cascade=CascadeType.PERSIST)
	@JoinColumn(name="video_id", referencedColumnName = "videoid")
	private Video video;

	@SuppressWarnings("unused")
	private VideoRate() {}

	public VideoRate(User user, Video video) {
		this.user = user;
		this.video = video;
	}

	public int getVideoRateId() {
		return videorateid;
	}

	public void setVideoRate(int videorate) {
		this.videorate = videorate;
	}

	public int getVideoRate() {
		return videorate;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Video getVideo() {
		return video;
	}

	public void setVideo(Video video) {
		this.video = video;
	}

}
