package insomniaclub.video.model;

import insomniaclub.authentication.model.User;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table (name = "videocomment")
public class VideoComment {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "commentid")
	private int commentid;
	
	@Column(name = "comment")
	private String comment;
	
	@ManyToOne(cascade=CascadeType.PERSIST)
	@JoinColumn(name="email", referencedColumnName = "email")
	private User user;
	
	@ManyToOne(cascade=CascadeType.PERSIST)
	@JoinColumn(name="video_id", referencedColumnName = "videoid")
	private Video video;
	
	@SuppressWarnings("unused")
	private VideoComment() {}
	public VideoComment(String videocomment, User user, Video video) {
		this.comment = videocomment;
		this.user = user;
		this.video = video;	
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	public int getCommentId() {
		return commentid;
	}
	
	public void setComment(String comment) {
		this.comment = comment;
	}
	
	public String getComment() {
		return comment;
	}
	
	public Video getVideo() {
		return video;
	}

	public void setVideo(Video video) {
		this.video = video;
	}
	
}
