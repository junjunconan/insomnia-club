package insomniaclub.video.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table (name = "videotag")
public class videotag {
	
	@Id
	@Column(name = "videotagid")
	private int videotagid;
	
	@Column(name = "videotagname")
	private String videotagname;
	

	public void setVideoTagId(int videotagid) {
		this.videotagid = videotagid;
	}
	
	public int getVideoTagId() {
		return videotagid;
	}
	
	public void setVideoTagName(String videotagname) {
		this.videotagname = videotagname;
	}
	
	public String getVideoTagName() {
		return videotagname;
	}
	
	
}
