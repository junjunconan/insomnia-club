package insomniaclub.suggestion.model;

import insomniaclub.authentication.model.User;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

@Entity
@Table(name="suggestioncomment")
public class SuggestionComment {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	private int id;

	@Column(name="comment")
	@NotNull
	private String comment;

	@ManyToOne(cascade=CascadeType.PERSIST)
	@JoinColumn(name="user_id", referencedColumnName = "email")
	private User user;

	@ManyToOne(cascade=CascadeType.PERSIST)
	@JoinColumn(name="suggestion_id", referencedColumnName = "id")
	private Suggestion suggestion;

	@Column(name="date")
	@Temporal(value=TemporalType.TIMESTAMP )
	@Generated(value=GenerationTime.INSERT)
	private Date date;

	@SuppressWarnings("unused")
	private SuggestionComment() {}

	public SuggestionComment(String comment, User user, Suggestion suggestion) {
		this.comment = comment;
		this.user = user;
		this.suggestion = suggestion;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Suggestion getSuggestion() {
		return suggestion;
	}

	public void setSuggestion(Suggestion suggestion) {
		this.suggestion = suggestion;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof SuggestionComment))
			return false;
		return id == ((SuggestionComment) object).id;
	}

}
