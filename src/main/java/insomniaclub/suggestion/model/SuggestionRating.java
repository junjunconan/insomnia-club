package insomniaclub.suggestion.model;

import insomniaclub.authentication.model.User;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

@Entity
@Table(name="suggestionrating")
public class SuggestionRating {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	private int id;

	@Column(name="rating")
	@NotNull
	private int rating;

	@ManyToOne(cascade=CascadeType.PERSIST)
	@JoinColumn(name="user_id", referencedColumnName = "email")
	private User user;

	@ManyToOne(cascade=CascadeType.PERSIST)
	@JoinColumn(name="suggestion_id", referencedColumnName = "id")
	private Suggestion suggestion;

	@Column(name="date")
	@Temporal(value=TemporalType.TIMESTAMP )
	@Generated(value=GenerationTime.INSERT)
	private Date date;

	@SuppressWarnings("unused")
	private SuggestionRating() {}

	public SuggestionRating(int rating, User user, Suggestion suggestion) {
		this.rating = rating;
		this.user = user;
		this.suggestion = suggestion;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Suggestion getSuggestion() {
		return suggestion;
	}

	public void setSuggestion(Suggestion suggestion) {
		this.suggestion = suggestion;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof SuggestionRating))
			return false;
		return id == ((SuggestionRating) object).id;
	}

}
