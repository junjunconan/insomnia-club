package insomniaclub.suggestion.service;

import insomniaclub.suggestion.model.Suggestion;

import java.util.List;

public interface ISuggestionDao {

	public void saveSuggestion(Suggestion suggestion);

	public void updateSuggestion(Suggestion suggestion);

	public void deleteSuggestion(Suggestion suggestion);

	public Suggestion findById(int id);

	public List<Suggestion> findAll();

	public List<Suggestion> randomFind();

}
