package insomniaclub.suggestion.service;

import insomniaclub.authentication.model.User;
import insomniaclub.suggestion.model.Suggestion;
import insomniaclub.suggestion.model.SuggestionRating;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;

@Transactional
public class SuggestionRatingDao implements ISuggestionRatingDao {

	@Autowired
	private HibernateTemplate hibernateTemplate;

	@Override
	public void saveSuggestionRating(SuggestionRating suggestionRating) {
		hibernateTemplate.save(suggestionRating);
	}

	@Override
	public void updateSuggestionRating(SuggestionRating suggestionRating) {
		hibernateTemplate.update(suggestionRating);
	}

	@Override
	public void deleteSuggestionRating(SuggestionRating suggestionRating) {
		hibernateTemplate.delete(suggestionRating);
	}

	@Override
	public List<SuggestionRating> findAll() {
		return hibernateTemplate.loadAll(SuggestionRating.class);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<SuggestionRating> findBySuggestionUser(Suggestion suggestion, User user) {
		String[] paramNames = {"suggestion", "user"};
		Object[] paramValues = {suggestion, user};
		return (List<SuggestionRating>) hibernateTemplate
				.findByNamedParam("FROM SuggestionRating s WHERE s.suggestion=:suggestion AND s.user=:user",
									paramNames, paramValues);
	}

	@Override
	@SuppressWarnings("rawtypes")
	public List orderByRating() {
		Session session = hibernateTemplate.getSessionFactory().getCurrentSession();
		Query query = session.createQuery("SELECT s.suggestion, avg(s.rating) FROM SuggestionRating s " +
				"GROUP BY s.suggestion " +
				"ORDER BY avg(s.rating) DESC").setMaxResults(10);
		return query.list();
	}

}
