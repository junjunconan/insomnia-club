package insomniaclub.suggestion.service;

import insomniaclub.suggestion.model.Suggestion;
import insomniaclub.suggestion.model.SuggestionComment;

import java.util.List;

public interface ISuggestionCommentDao {

	public void saveSuggestionComment(SuggestionComment suggestionComment);

	public void updateSuggestionComment(SuggestionComment suggestionComment);

	public void deleteSuggestionComment(SuggestionComment suggestionComment);

	public List<SuggestionComment> findAll();

	public List<SuggestionComment> findBySuggestion(Suggestion suggestion);

}
