package insomniaclub.suggestion.service;

import insomniaclub.suggestion.model.Suggestion;
import insomniaclub.suggestion.model.SuggestionComment;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;

@Transactional
public class SuggestionCommentDao implements ISuggestionCommentDao {

	@Autowired
	private HibernateTemplate hibernateTemplate;

	@Override
	public void saveSuggestionComment(SuggestionComment suggestionComment) {
		hibernateTemplate.save(suggestionComment);
	}

	@Override
	public void updateSuggestionComment(SuggestionComment suggestionComment) {
		hibernateTemplate.update(suggestionComment);
	}

	@Override
	public void deleteSuggestionComment(SuggestionComment suggestionComment) {
		hibernateTemplate.delete(suggestionComment);
	}

	@Override
	public List<SuggestionComment> findAll() {
		return hibernateTemplate.loadAll(SuggestionComment.class);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<SuggestionComment> findBySuggestion(Suggestion suggestion) {
		return (List<SuggestionComment>) hibernateTemplate
				.findByNamedParam("FROM SuggestionComment s WHERE s.suggestion=:suggestion",
									"suggestion", suggestion);
	}

}
