package insomniaclub.suggestion.service;

import insomniaclub.authentication.model.User;
import insomniaclub.suggestion.model.Suggestion;
import insomniaclub.suggestion.model.SuggestionRating;

import java.util.List;

public interface ISuggestionRatingDao {

	public void saveSuggestionRating(SuggestionRating suggestionRating);

	public void updateSuggestionRating(SuggestionRating suggestionRating);

	public void deleteSuggestionRating(SuggestionRating suggestionRating);

	public List<SuggestionRating> findAll();

	public List<SuggestionRating> findBySuggestionUser(Suggestion suggestion, User user);

	@SuppressWarnings("rawtypes")
	public List orderByRating();

}
