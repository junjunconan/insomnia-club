package insomniaclub.suggestion.service;

import insomniaclub.suggestion.model.Suggestion;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;

@Transactional
public class SuggestionDao implements ISuggestionDao {

	@Autowired
	private HibernateTemplate hibernateTemplate;

	@Override
	public void saveSuggestion(Suggestion suggestion) {
		hibernateTemplate.save(suggestion);
	}

	@Override
	public void updateSuggestion(Suggestion suggestion) {
		hibernateTemplate.update(suggestion);
	}

	@Override
	public void deleteSuggestion(Suggestion suggestion) {
		hibernateTemplate.delete(suggestion);
	}

	@Override
	public Suggestion findById(int id) {
		return hibernateTemplate.get(Suggestion.class, id);
	}

	@Override
	public List<Suggestion> findAll() {
		return hibernateTemplate.loadAll(Suggestion.class);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Suggestion> randomFind() {
		Session session = hibernateTemplate.getSessionFactory().getCurrentSession();
		Query query = session.createQuery("FROM Suggestion s order by rand()").setMaxResults(10);
		return query.list();
	}

}
