package insomniaclub.suggestion.controller;

import insomniaclub.authentication.model.User;
import insomniaclub.suggestion.model.Suggestion;
import insomniaclub.suggestion.service.ISuggestionDao;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@Scope("session")
@RequestMapping("/insomniaclub/")
public class ProposeController {

	@Autowired
	private ISuggestionDao suggestionDao;

	@RequestMapping(value = "propose")
	public String propose(HttpSession session, Model model) {
		if (session.getAttribute("user") == null)
			return "redirect:/insomniaclub";
		return "propose";
	}

	@RequestMapping(value = "propose", method = RequestMethod.POST)
	public String proposeAction(@ModelAttribute("proposemodel") ProposeModel proposeModel,
			HttpSession session) {
		User user;
		if ((user = (User) session.getAttribute("user")) == null)
			return "redirect:/insomniaclub";

		Suggestion suggestion = new Suggestion(proposeModel.title, proposeModel.description,
												user);
		suggestionDao.saveSuggestion(suggestion);

		return "redirect:/insomniaclub";
	}

	@SuppressWarnings("unused")
	private static class ProposeModel {

		private String title;
		private String description;

		public void setTitle(String title) {
			this.title = title;
		}

		public void setDescription(String description) {
			this.description = description;
		}


	}

}
