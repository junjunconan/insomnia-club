package insomniaclub.suggestion.controller;

import insomniaclub.authentication.model.User;
import insomniaclub.suggestion.model.Suggestion;
import insomniaclub.suggestion.model.SuggestionComment;
import insomniaclub.suggestion.model.SuggestionRating;
import insomniaclub.suggestion.service.ISuggestionCommentDao;
import insomniaclub.suggestion.service.ISuggestionDao;
import insomniaclub.suggestion.service.ISuggestionRatingDao;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.fasterxml.jackson.databind.ObjectMapper;

@Controller
@Scope("session")
@RequestMapping("/insomniaclub/")
public class SuggestionController {

	@Autowired
	private ISuggestionDao suggestionDao;
	@Autowired
	private ISuggestionRatingDao srDao;
	@Autowired
	private ISuggestionCommentDao scDao;

	@RequestMapping(value = "suggestion")
	public String suggestion(HttpSession session, Model model) {
		User user;
		if ((user = (User) session.getAttribute("user")) == null)
			return "redirect:/insomniaclub";

		List<Suggestion> suggestions = suggestionDao.randomFind();
		List<Integer> list = new ArrayList<Integer>();
		List<SuggestionModel> result = new ArrayList<SuggestionModel>();
		for (Suggestion s : suggestions) {
			SuggestionModel suggestionm = new SuggestionModel();
			suggestionm.id = s.getId();
			suggestionm.user = s.getUser();
			suggestionm.title = s.getTitle();
			suggestionm.description = s.getDescription();
			list.add(s.getId());
			List<SuggestionRating> ratings = srDao.findBySuggestionUser(s, user);
			if (ratings.size() > 0)
				suggestionm.rating = ratings.get(0).getRating();
			else
				suggestionm.rating = -1;
			result.add(suggestionm);
		}
		model.addAttribute("suggestions", result);

		ObjectMapper mapper = new ObjectMapper();
		String json="";
		try {
			json = mapper.writeValueAsString(list);
		} catch (Exception e) {
			e.printStackTrace();
		}
		model.addAttribute("list", json);

		return "suggestion";
	}

	@RequestMapping(value = "suggestion/{id}")
	public String suggestionDetail(HttpSession session, Model model,
			@PathVariable int id) {
		User user;
		if ((user = (User) session.getAttribute("user")) == null)
			return "redirect:/insomniaclub";

		Suggestion suggestion = suggestionDao.findById(id);
		if (suggestion == null)
			return "redirect:/insomniaclub";

		List<SuggestionComment> scs = scDao.findBySuggestion(suggestion);
		List<CommentModel> result = new ArrayList<CommentModel>();
		List<Integer> commentList = new ArrayList<Integer>();
		for (SuggestionComment sc : scs) {
			CommentModel commentm = new CommentModel();
			commentm.id = sc.getId();
			commentm.comment = sc.getComment();
			commentm.user = sc.getUser();
			commentm.rating = srDao.findBySuggestionUser(suggestion, sc.getUser())
					.get(0).getRating();
			result.add(commentm);
			commentList.add(sc.getId());
		}

		ObjectMapper mapper = new ObjectMapper();
		String json="";
		try {
			json = mapper.writeValueAsString(commentList);
		} catch (Exception e) {
			e.printStackTrace();
		}
		model.addAttribute("commentlist", json);
		model.addAttribute("suggestion", suggestion);
		model.addAttribute("result", result);

		List<SuggestionRating> ratings = srDao.findBySuggestionUser(suggestion, user);
		if (ratings.size() > 0)
			model.addAttribute("rated", ratings.get(0).getRating());
		else
			model.addAttribute("rated", -1);

		return "suggestion_detail";
	}

	@RequestMapping(value = "suggestion/{id}", method = RequestMethod.POST)
	public String commentOnSuggestion(@ModelAttribute("comment") CommentModel commentModel,
			HttpSession session, @PathVariable int id) {
		User user;
		if ((user = (User) session.getAttribute("user")) == null)
			return "redirect:/insomniaclub";

		Suggestion suggestion = suggestionDao.findById(id);
		if (suggestion == null)
			return "redirect:/insomniaclub";

		List<SuggestionRating> ratings = srDao.findBySuggestionUser(suggestion, user);

		if (suggestion.getUser().equals(user)) {
			if (ratings.size() == 0) {
				SuggestionRating suggestionRating = new SuggestionRating(commentModel.rating,
						suggestion.getUser(), suggestion);
				srDao.saveSuggestionRating(suggestionRating);
			}

			SuggestionComment suggestionComment = new SuggestionComment(commentModel.comment,
					suggestion.getUser(), suggestion);
			scDao.saveSuggestionComment(suggestionComment);
		}
		else {
			if (ratings.size() == 0) {
				SuggestionRating suggestionRating = new SuggestionRating(commentModel.rating,
						user, suggestion);
				srDao.saveSuggestionRating(suggestionRating);
			}

			SuggestionComment suggestionComment = new SuggestionComment(commentModel.comment,
					user, suggestion);
			scDao.saveSuggestionComment(suggestionComment);
		}

		return "redirect:/insomniaclub/suggestion/" + id;
	}

	@SuppressWarnings("unused")
	private static class SuggestionModel {

		private int id;
		private User user;
		private String title;
		private String description;
		private int rating;

		public int getId() {
			return id;
		}

		public User getUser() {
			return user;
		}

		public String getTitle() {
			return title;
		}

		public String getDescription() {
			return description;
		}

		public int getRating() {
			return rating;
		}

	}

	@SuppressWarnings("unused")
	private static class CommentModel {

		private int id;
		private User user;
		private int rating;
		private String comment;

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public User getUser() {
			return user;
		}

		public void setUser(User user) {
			this.user = user;
		}

		public int getRating() {
			return rating;
		}

		public void setRating(int rating) {
			this.rating = rating;
		}

		public String getComment() {
			return comment;
		}

		public void setComment(String comment) {
			this.comment = comment;
		}

	}

}
