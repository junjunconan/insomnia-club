package insomniaclub.music.controller;

import insomniaclub.authentication.model.User;
import insomniaclub.music.model.Music;
import insomniaclub.music.service.IMusicDao;

import java.io.File;
import java.nio.file.Files;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;


@Controller
@Scope("session")
@RequestMapping("/insomniaclub")
public class AddMusicController {

	@Autowired
	private IMusicDao musicdao;

	@RequestMapping(value = "/addmusic")
	public String music(HttpSession session){
		if (session.getAttribute("user") == null)
			return "redirect:/insomniaclub";
		return "musicinfo";
	}

	@RequestMapping(value = "/addmusic", method = RequestMethod.POST)
	public String musicAction(@ModelAttribute("usermodel") MusicModel musicModel,
			@RequestParam("file") MultipartFile file, HttpSession session) throws Exception {
		User user;
		if ((user = (User) session.getAttribute("user")) == null)
			return "redirect:/insomniaclub";

		File dir = new File("music");
		if (!dir.isDirectory())
			dir.mkdirs();
		File uploadedFile = new File(dir, file.getOriginalFilename());
		Files.copy(file.getInputStream(), uploadedFile.toPath());

		Music music = new Music();
		music.setMusicname(musicModel.musicName);
		music.setMusictype(musicModel.musicType);
		music.setSinger(musicModel.singer);
		music.setKeyword1(musicModel.keyword1);
		music.setKeyword2(musicModel.keyword2);
		music.setKeyword3(musicModel.keyword3);
		music.setUser(user);
		music.setFilename(file.getOriginalFilename());

		musicdao.saveMusic(music);

		return "redirect:/insomniaclub/music";
	}

	@SuppressWarnings("unused")
	public
	static class MusicModel {

		private int id;
		private String musicName;
		private String musicType;
		private String singer;
		private String keyword1;
		private String keyword2;
		private String keyword3;

		public void setId(int id) {
			this.id = id;
		}

		public void setMusicName(String musicname) {
			this.musicName = musicname;
		}

		public void setSinger(String singer){
			this.singer = singer;
		}

		public void setMusicType(String musictype){
			this.musicType = musictype;
		}

		public void setKeyword1(String keyword1){
			this.keyword1 = keyword1;
		}

		public void setKeyword2 (String keyword2){
			this.keyword2 = keyword2;
		}

		public void setKeyword3 (String keyword3){
			this.keyword3 = keyword3;
		}

	}


}