package insomniaclub.music.controller;

import insomniaclub.music.service.IMusicDao;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@Scope("session")
@RequestMapping("/insomniaclub")
public class MusicSearchController {

	@Autowired
	private IMusicDao musicdao;

	@RequestMapping(value = "/music", method = RequestMethod.POST)
	public String searchMusic(@RequestParam("keyword") String keyword, HttpSession session, Model model){
		if (session.getAttribute("user") == null)
			return "redirect:/insomniaclub";

		model.addAttribute("musiclist", musicdao.findByMusicName(keyword));

		return "musiclist";
	}

}
