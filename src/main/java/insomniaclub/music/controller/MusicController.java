package insomniaclub.music.controller;

import insomniaclub.music.model.Music;
import insomniaclub.music.service.IMusicDao;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@Scope("session")
@RequestMapping("/insomniaclub")
public class MusicController {

	@Autowired
	private IMusicDao musicdao;

	@RequestMapping(value = "/music")
	public String getAllMusic(HttpSession session, Model model){
		if (session.getAttribute("user") == null)
			return "redirect:/insomniaclub";

		model.addAttribute("musiclist", musicdao.findAll());

		return "musiclist";
	}

	@RequestMapping("/music/{id}")
	public void musicPlay(HttpServletResponse response, @PathVariable int id)
			throws Exception {
		Music music = musicdao.findById(id);
		File file = new File("music", music.getFilename());
		InputStream instream = new FileInputStream(file);
		IOUtils.copy(instream, response.getOutputStream());
	}

}
