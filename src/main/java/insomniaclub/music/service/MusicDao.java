package insomniaclub.music.service;

import insomniaclub.music.model.Music;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;

@Transactional
public class MusicDao implements IMusicDao {

	@Autowired
	private HibernateTemplate hibernateTemplate;

	@Override
	public void saveMusic(Music music){
		hibernateTemplate.save(music);
	}

	@Override
	public void deleteMusic(Music music){
		hibernateTemplate.delete(music);
	}

	@Override
	public List<Music> findAll() {
		return hibernateTemplate.loadAll(Music.class);
	}

	@Override
	public Music findById(int id) {
		return hibernateTemplate.get(Music.class, id);
	}

	@Override
	public List<Music> findByMusicName(String name) {
		return (List<Music>) hibernateTemplate
				.findByNamedParam("FROM Music m WHERE m.musicname=:musicname", "musicname", name);
	}

}
