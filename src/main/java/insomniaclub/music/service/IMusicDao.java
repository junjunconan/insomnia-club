package insomniaclub.music.service;

import insomniaclub.music.model.Music;

import java.util.List;

public interface IMusicDao {

	public void saveMusic(Music music);

	public void deleteMusic(Music music);

	public List<Music> findAll();

	public Music findById(int id);

	public List<Music> findByMusicName(String name);

}
