package insomniaclub.music.service;

import insomniaclub.music.model.MusicRating;

public interface IMusicRatingDao {

	public void saveMusicRating(MusicRating musicRating);

	public void updateMusicRating(MusicRating musicRating);

	public void deleteMusicRating(MusicRating musicRating);


}
