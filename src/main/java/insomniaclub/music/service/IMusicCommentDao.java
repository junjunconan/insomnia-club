package insomniaclub.music.service;

import insomniaclub.music.model.MusicComment;

public interface IMusicCommentDao {

	public void saveMusicComment(MusicComment musicComment);

	public void updateMusicComment(MusicComment musicComment);

	public void deleteMusicComment(MusicComment musicComment);


}
