package insomniaclub.music.service;

import insomniaclub.music.model.MusicComment;
import insomniaclub.music.service.IMusicCommentDao;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;

@Transactional
public class MusicCommentDao implements IMusicCommentDao {

	@Autowired
	private HibernateTemplate hibernateTemplate;

	@Override
	public void saveMusicComment(MusicComment musicComment) {
		hibernateTemplate.save(musicComment);
	}

	@Override
	public void updateMusicComment(MusicComment musicComment) {
		hibernateTemplate.update(musicComment);
	}

	@Override
	public void deleteMusicComment(MusicComment musicComment) {
		hibernateTemplate.delete(musicComment);
	}

}

