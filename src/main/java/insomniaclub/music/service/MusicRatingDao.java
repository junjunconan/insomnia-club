package insomniaclub.music.service;

import insomniaclub.music.model.MusicRating;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;

@Transactional
public class MusicRatingDao implements IMusicRatingDao {

	@Autowired
	private HibernateTemplate hibernateTemplate;

	@Override
	public void saveMusicRating(MusicRating musicRating) {
		hibernateTemplate.save(musicRating);
	}

	@Override
	public void updateMusicRating(MusicRating musicRating) {
		hibernateTemplate.update(musicRating);
	}

	@Override
	public void deleteMusicRating(MusicRating musicRating) {
		hibernateTemplate.delete(musicRating);
	}

}
