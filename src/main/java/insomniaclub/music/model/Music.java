package insomniaclub.music.model;

import insomniaclub.authentication.model.User;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table (name = "music")
public class Music {

	@Id
	@Column(name = "id")
	private int id;

	@Column(name = "musicname")
	private String musicname;

	@Column(name = "musictype")
	private String musictype;

	@Column(name = "singer")
	private String singer;

	@Column(name = "keyword1")
	private String keyword1;

	@Column(name = "keyword2")
	private String keyword2;

	@Column(name = "keyword3")
	private String keyword3;

	@ManyToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name = "email")
	private User user;
	
	@Column(name = "filename")
	private String filename;

	public Music() {}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setMusicname(String musicname){
		this.musicname = musicname;
	}

	public String getMusicname(){
		return musicname;
	}

	public void setMusictype(String musictype){
		this.musictype = musictype;
	}

	public String getMusictype(){
		return musictype;
	}

	public void setSinger(String singer){
		this.singer = singer;
	}

	public String getSinger(){
		return singer;
	}

	public void setKeyword1(String keyword1){
		this.keyword1 = keyword1;
	}

	public String getKeyword1(){
		return keyword1;
	}

	public void setKeyword2(String keyword2){
		this.keyword2 = keyword2;
	}

	public String getKeyword2(){
		return keyword2;
	}

	public void setKeyword3(String keyword3){
		this.keyword3 = keyword3;
	}

	public String getKeyword3(){
		return keyword3;
	}

	public void setFilename(String filename){
		this.filename = filename;
	}

	public String getFilename() {
		return filename;
	}

}
