package insomniaclub.music.model;

import insomniaclub.authentication.model.User;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table (name= "musicranking")
public class MusicRating {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	private int id;

	@Column(name="rating")
	@NotNull
	private int rating;

	@ManyToOne(cascade=CascadeType.PERSIST)
	@JoinColumn(name="user_id", referencedColumnName = "email")
	private User user;

	@ManyToOne(cascade=CascadeType.PERSIST)
	@JoinColumn(name="music_id", referencedColumnName = "id")
	private Music music;

	@SuppressWarnings("unused")
	private MusicRating() {}

	public MusicRating (int rating, User user, Music music){
		this.rating = rating;
		this.user = user;
		this.music = music;
	}

	public int getId() {
		return id;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getRating() {
		return rating;
	}

	public void setRaking(int rating) {
		this.rating = rating;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Music getMusic() {
		return music;
	}

	public void setMusic(Music music){
		this.music = music;
	}
}
