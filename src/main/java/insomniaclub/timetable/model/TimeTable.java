package insomniaclub.timetable.model;
import insomniaclub.authentication.model.User;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="timetable")
public class TimeTable{

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id")
	private int id;

	@ManyToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name = "email", referencedColumnName = "email", unique = true)
	private User email;

	@Column(name="eventname")
	private String eventName;

	@Column(name="eventtime")
	private String eventTime;

	public TimeTable(){

	}
	public TimeTable(User email, String eventName, String eventTime) {
		this.eventName = eventName;
		this.eventTime = eventTime;
		this.email = email;
	}

	public User getEmail() {
		return email;
	}

	public void setEmail(User email) {
		this.email = email;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public String getEventTime() {
		return eventTime;
	}

	public void setEvenTime(String eventTime) {
		this.eventTime = eventTime;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}


	@Override
	public boolean equals(Object object) {
		if(!(object instanceof TimeTable))
			return false;
		return id == ((TimeTable) object).id;
	}

}

