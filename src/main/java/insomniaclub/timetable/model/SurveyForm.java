package insomniaclub.timetable.model;

import insomniaclub.authentication.model.User;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table (name = "surveyform")
public class SurveyForm {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private int id;

	@OneToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name = "email", unique = true)
	private User email;

	@Column(name = "job")
	private String job;

	@Column(name = "gtime")
	private String gTime;

	@Column(name = "btime")
	private String bTime;

	@Column(name = "typemusic")
	private String typeMusic;

	@Column(name = "aexercise")
	private boolean aExercise;

	@Column(name = "aemail")
	private boolean aEmail;


	public SurveyForm() {

	}

	public void setEmail(User email) {
		this.email = email;
	}

	public User getEmail() {
		return email;
	}

	public void setJob(String job) {
		this.job = job;
	}

	public String getJob() {
		return job;
	}

	public void setGTime(String gTime) {
		this.gTime = gTime;
	}

	public String getGTime() {
		return gTime;
	}

	public void setBTime(String bTime) {
		this.bTime = bTime;
	}

	public String getBTime() {
		return bTime;
	}

	public void setTypeMusic(String typeMusic) {
		this.typeMusic = typeMusic;
	}

	public String getTypeMusic() {
		return typeMusic;
	}

	public void setAExercise(boolean aExercise) {
		this.aExercise = aExercise;
	}

	public boolean getAExercise() {
		return aExercise;
	}

	public void setAEmail(boolean aEmail){
		this.aEmail = aEmail;
	}

	public boolean getAEmail() {
		return aEmail;
	}

	@Override
	public boolean equals(Object object) {
		if(!(object instanceof SurveyForm))
			return false;
		return id == ((SurveyForm)object).id;
	}
}
