package insomniaclub.timetable.model;

import insomniaclub.authentication.model.User;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table (name = "eventcheck")
public class EventCheck {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private int id;

	@ManyToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name="email", referencedColumnName = "email")
	private User email;

	@Column(name = "event")
	private String eventName;

	@Column(name = "eventdate")
	private Date eventTime;

	public EventCheck(){

	}
	public EventCheck (User email, String eventName, Date eventTime) {
		this.email = email;
		this.eventName = eventName;
		this.eventTime = eventTime;
	}

	public void setEmail(User email) {
		this.email = email;
	}

	public User getEmail() {
		return email;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventTime(Date eventTime) {
		this.eventTime = eventTime;
	}

	public Date getEventTime() {
		return eventTime;
	}

	@Override
	public boolean equals(Object object) {
		if(!(object instanceof EventCheck))
			return false;
		return id == ((EventCheck) object).id;
	}
}
