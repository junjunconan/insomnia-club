package insomniaclub.timetable.service;

import insomniaclub.authentication.model.User;
import insomniaclub.timetable.model.EventCheck;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;


@Transactional
public class EventCheckDao implements IEventCheckDao{

	@Autowired
	private HibernateTemplate hibernateTemplate;

	@Override
	public void saveEventCheck(EventCheck eventcheck) {
		// TODO Auto-generated method stub
		hibernateTemplate.saveOrUpdate(eventcheck);
	}

	@Override
	public void deleteEventCheck(EventCheck eventcheck) {
		// TODO Auto-generated method stub
		hibernateTemplate.delete(eventcheck);

	}

	@Override
	public List<EventCheck> findAll() {
		return hibernateTemplate.loadAll(EventCheck.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<EventCheck> findByEmail(User email){
		return (List<EventCheck>) hibernateTemplate.find("FROM EventCheck e WHERE e.email=?", email);
	}

}
