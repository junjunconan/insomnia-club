package insomniaclub.timetable.service;

import insomniaclub.authentication.model.User;
import insomniaclub.timetable.model.SurveyForm;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;

@Transactional

public class SurveyFormDao implements ISurveyFormDao {

	@Autowired
	private HibernateTemplate hibernateTemplate;

	@Override
	public void saveSurveyForm (SurveyForm surveyform) {
		hibernateTemplate.saveOrUpdate(surveyform);
	}

	@Override
	public void deleteSurveyForm (SurveyForm surveyform) {
		hibernateTemplate.delete(surveyform);
	}

	@Override
	public SurveyForm findById(int id) {
		return hibernateTemplate.get(SurveyForm.class, id);
	}

	@Override
	public List<SurveyForm> findAll(){
		return hibernateTemplate.loadAll(SurveyForm.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SurveyForm> findByEmail(User email) {
		return (List<SurveyForm>) hibernateTemplate.find("FROM SurveyForm s WHERE s.email=?", email);
	}//hibernateTemplate.find //.get only can find private key

}
