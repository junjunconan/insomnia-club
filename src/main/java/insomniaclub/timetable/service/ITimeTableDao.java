package insomniaclub.timetable.service;

import insomniaclub.authentication.model.User;
import insomniaclub.timetable.model.TimeTable;

import java.util.List;

public interface ITimeTableDao {

	public void saveTimeTable(TimeTable timetable);

	public void deleteTimeTable(TimeTable timetable);

	public List<TimeTable> findAll();

	public List<TimeTable> findByEmail(User email);

	public List<TimeTable> findByUserEvent(User email, String eventName);
	
	TimeTable findById(int id);

}
