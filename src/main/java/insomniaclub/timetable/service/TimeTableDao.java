package insomniaclub.timetable.service;

import insomniaclub.authentication.model.User;
import insomniaclub.timetable.model.TimeTable;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;

@Transactional
public class TimeTableDao implements ITimeTableDao {

	@Autowired
	private HibernateTemplate hibernateTemplate;

	@Override
	public void saveTimeTable (TimeTable timetable) {
		hibernateTemplate.saveOrUpdate(timetable);
	}

	@Override
	public void deleteTimeTable (TimeTable timetable) {
		hibernateTemplate.delete(timetable);
	}

	@Override
	public TimeTable findById(int id){
		return hibernateTemplate.get(TimeTable.class, id);
	}

	@Override
	public List<TimeTable> findAll() {
		return hibernateTemplate.loadAll(TimeTable.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TimeTable> findByEmail(User email) {
		// TODO Auto-generated method stub
		return (List<TimeTable>) hibernateTemplate.find("FROM TimeTable t WHERE t.email=?", email);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TimeTable> findByUserEvent(User email, String eventName) {
		String[] paramNames = {"email", "eventName"};
		Object[] paramValues = {email, eventName};
		return (List<TimeTable>) hibernateTemplate
				.findByNamedParam("FROM TimeTable t WHERE t.email=:email AND t.eventName=:eventName",
									paramNames, paramValues);
	}

}
