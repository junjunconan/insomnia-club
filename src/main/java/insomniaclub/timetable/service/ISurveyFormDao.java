package insomniaclub.timetable.service;

import insomniaclub.authentication.model.User;
import insomniaclub.timetable.model.SurveyForm;

import java.util.List;

public interface ISurveyFormDao {

	public void saveSurveyForm(SurveyForm surveyform);

	public void deleteSurveyForm(SurveyForm surveyform);

	public SurveyForm findById(int id);

	public List<SurveyForm> findByEmail(User email);

	List<SurveyForm> findAll();

}
