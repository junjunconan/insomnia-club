package insomniaclub.timetable.service;

import insomniaclub.authentication.model.User;
import insomniaclub.timetable.model.EventCheck;

import java.util.List;

public interface IEventCheckDao {

	public void saveEventCheck(EventCheck eventcheck);

	public void deleteEventCheck(EventCheck eventcheck);

	public List<EventCheck> findAll();

	public List<EventCheck> findByEmail(User email);

}
