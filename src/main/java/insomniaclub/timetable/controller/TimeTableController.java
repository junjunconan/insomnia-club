package insomniaclub.timetable.controller;

import java.util.ArrayList;
import java.util.List;

import insomniaclub.authentication.model.User;
import insomniaclub.timetable.model.TimeTable;
import insomniaclub.timetable.service.ITimeTableDao;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.fasterxml.jackson.databind.ObjectMapper;

@Controller
@Scope("session")
@RequestMapping("/insomniaclub/")
public class TimeTableController {
	@Autowired
	private ITimeTableDao ttdao;

	@RequestMapping(value = "timetable")
	public String timetable(HttpSession session, Model model){
		User user;
		@SuppressWarnings("unused")
		TimeTable timetableuser;
		if((user = (User)session.getAttribute("user")) == null)
		{
			return "redirect:/insomniaclub";

		}
		else if(ttdao.findByEmail(user).size()==0){
			return "redirect:/insomniaclub/surveyform";
		}
		else{
			List<TimeTable> timetable = ttdao.findByEmail(user);
			List<Integer> list = new ArrayList<Integer>();
			for(TimeTable t : timetable){
				list.add(t.getId());
			}
			model.addAttribute("timetable", timetable);
			ObjectMapper mapper = new ObjectMapper();
			String json="";
			try {
				json = mapper.writeValueAsString(list);
			} catch (Exception e) {
				e.printStackTrace();
			}
			model.addAttribute("list", json);
			return "timetable";
		}
	}

	@SuppressWarnings("unused")
	private static class TimeTableModel{
		private String email;
		private String eventName;
		private String eventTime;
		public void setEmail(String email){
			this.email = email;
		}
		public void setEventName(String eventName){
			this.eventName = eventName;
		}
		public void setEventTime(String eventTime){
			this.eventTime = eventTime;
		}
		public String getEmail(){
			return email;
		}
		public String getEventName(){
			return eventName;
		}
		public String getEventTime(){
			return eventTime;
		}
	}
}
