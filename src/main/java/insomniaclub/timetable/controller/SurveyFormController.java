package insomniaclub.timetable.controller;

import insomniaclub.authentication.model.User;
import insomniaclub.authentication.service.IUserDao;
import insomniaclub.timetable.model.SurveyForm;
import insomniaclub.timetable.service.ISurveyFormDao;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@Scope("session")
@RequestMapping("/insomniaclub/")
public class SurveyFormController {
	@Autowired
	private ISurveyFormDao sfdao;
	@Autowired
	private IUserDao udao;

	@RequestMapping(value = "surveyform")
	public String surveyform(HttpSession session){
		if(session.getAttribute("user") != null)
			return "surveyform";
		return"signup";
		//search the timetable database if there have timetable return checktimetable
		//return "checktimetable";
	}

	@RequestMapping(value = "surveyform", method = RequestMethod.POST)
	public String surveyformAction(@ModelAttribute("surveyformmodel") SurveyFormModel surveyformModel,
			HttpSession session){
		User user;
		if ((user = (User) session.getAttribute("user")) == null)
			return "redirect:/insomniaclub";

		if(session.getAttribute("surveyform") != null)
			return "redirect:/insomniaclub/suggesttimetable";

		if (surveyformModel.age == ""||surveyformModel.job =="" ||
				surveyformModel.job == null || surveyformModel.gender == "" ||
				surveyformModel.gender == null || surveyformModel.gTime == "" ||
				surveyformModel.gTime == null || surveyformModel.bTime == "" ||
				surveyformModel.bTime == null ||surveyformModel.typeMusic == "" ||
				surveyformModel.typeMusic == null){
			return "redirect:/insomniaclub/surveyform";
		}
		else{
			user.setGender(surveyformModel.gender);
			user.setAge(Integer.parseInt(surveyformModel.age));
			udao.updateUser(user);

			List<SurveyForm> sfs = sfdao.findByEmail(user);
			SurveyForm surveyform = null;
			if (sfs.size() > 0) {
				surveyform = sfs.get(0);
			}
			else {
				surveyform = new SurveyForm();
			}
			surveyform.setEmail(user);
			surveyform.setJob(surveyformModel.job);
			surveyform.setGTime(surveyformModel.gTime);
			surveyform.setBTime(surveyformModel.bTime);
			surveyform.setTypeMusic(surveyformModel.typeMusic);
			surveyform.setAExercise(surveyformModel.aExercise);
			surveyform.setAEmail(surveyformModel.aEmail);
			sfdao.saveSurveyForm(surveyform);

			return "redirect:/insomniaclub/suggesttimetable";
		}
	}

	/****/
	/**get value from form**/
	@SuppressWarnings("unused")
	private static class SurveyFormModel{

		private String gender;
		private String age;
		private String job;
		private String gTime;
		private String bTime;
		private boolean aExercise;
		private String typeMusic;
		private boolean aEmail;

		public void setGender(String gender){
			this.gender=gender;
		}
		public void setAge(String age){
			this.age=age;
		}
		public void setJob(String job){
			this.job=job;
		}
		public void setGTime(String gTime){
			this.gTime = gTime;
		}
		public void setBTime(String bTime){
			this.bTime = bTime;
		}
		public void setAExercise(boolean aExercise){
			this.aExercise = aExercise;
		}
		public void setTypeMusic(String typeMusic){
			this.typeMusic = typeMusic;
		}

		public void setAEmail(boolean aEmail){
			this.aEmail = aEmail;
		}
	}

}
