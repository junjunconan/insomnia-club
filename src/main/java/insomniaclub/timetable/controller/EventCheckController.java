package insomniaclub.timetable.controller;
import insomniaclub.authentication.model.User;
import insomniaclub.timetable.model.EventCheck;
import insomniaclub.timetable.model.TimeTable;
import insomniaclub.timetable.service.IEventCheckDao;
import insomniaclub.timetable.service.ITimeTableDao;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@SuppressWarnings("unused")
@Controller
@Scope("session")
@RequestMapping("/insomniaclub/")

public class EventCheckController {
	@Autowired
	private ITimeTableDao ttdao;
	@Autowired
	private IEventCheckDao ecdao;
	@RequestMapping(value = "eventcheck")
	public String eventcheck(HttpSession session, Model model){
		User user;
	//	TimeTable timetable = null;
		if((user = (User)session.getAttribute("user"))==null){
			return "redirect:/insomniaclub";
		}
		else{
			if(ttdao.findByEmail(user).size()==0){
				return "redirect:/insomniaclub/surveyform";
			}
			else{
				DateFormat df = new SimpleDateFormat("HH:mm:ss");
				df.setTimeZone(TimeZone.getTimeZone("Australia/Sydney"));
			//	SimpleDateFormat dfdate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				Calendar c = Calendar.getInstance();
				Calendar a = Calendar.getInstance();
				String eventName = null;
				Date eventTime = null;
				Date SystemTime = null;
				Date beforeTimeDate = null;
				Date afterTimeDate = null;
				String eventDate = null;
				Date eventchangedate = null;
				List<String> listshow = new ArrayList<String>();
				List<TimeTable> timetable = ttdao.findByEmail(user);
				List<TimeTable> timetableshow = new ArrayList<TimeTable>();
				List<EventCheck> eventcheck = ecdao.findByEmail(user);
				List<Integer> list = new ArrayList<Integer>();
				
				for(TimeTable t: timetable){
					listshow.add(t.getEventTime().trim());
				}
				
				String systemtime = df.format(new Date());
				EventCheckModel eventCheckModel = new EventCheckModel();
				eventCheckModel.systemTime = systemtime;
				try {
					SystemTime = df.parse(systemtime);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				c.setTime(SystemTime);
				c.add(Calendar.HOUR, -1);
				String beforeTime = df.format(c.getTime());
				a.setTime(SystemTime);
				a.add(Calendar.HOUR, 1);
				String afterTime = df.format(a.getTime());
				try {
					beforeTimeDate = df.parse(beforeTime);
					afterTimeDate = df.parse(afterTime);
					
				} catch (ParseException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				for(int i = 0; i<listshow.size(); i++){
					boolean judgedate = false;
					try {
						eventTime = df.parse(timetable.get(i).getEventTime());
						eventName = timetable.get(i).getEventName();
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					if(eventTime.before(afterTimeDate)&&eventTime.after(beforeTimeDate)){
						list.add(timetable.get(i).getId());
						SimpleDateFormat dfdate = new SimpleDateFormat("yyyy-MM-dd");
						String systemdate = dfdate.format(new Date());
						for(int j = 0; j<eventcheck.size(); j ++){
							 eventDate = dfdate.format(eventcheck.get(j).getEventTime());
							 String eventtest= eventcheck.get(j).getEventName();
							 String eventtimetabletest = timetable.get(i).getEventName();
							if(eventtest.equalsIgnoreCase(eventtimetabletest)&&eventDate.equals(systemdate)){
								judgedate = true;
							}
						}
						if(judgedate == false){
						eventCheckModel.eventName = timetable.get(i).getEventName();
						eventCheckModel.eventTime = timetable.get(i).getEventTime();
						TimeTable timeshow = new TimeTable(timetable.get(i).getEmail(),timetable.get(i).getEventName(),timetable.get(i).getEventTime());
					    timetableshow.add(timeshow);
						}
					}
				}
				eventCheckModel.beforeTime = beforeTime;
				eventCheckModel.afterTime = afterTime;
				model.addAttribute("timetable", timetableshow);
				
				model.addAttribute("eventcheck", eventCheckModel);
				ObjectMapper mapper = new ObjectMapper();
				String json="";
				try {
					json = mapper.writeValueAsString(list);
				} catch (Exception e) {
					e.printStackTrace();
				}
				model.addAttribute("list", json);
				return "eventcheck";
			}
		}
	}
	@RequestMapping(value = "eventcheck", method = RequestMethod.POST)
	public String saveeventcheck(@ModelAttribute("eventcheckmodel")EventCheckModel eventcheckModel,
			HttpSession session){
		SimpleDateFormat dfdate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String savedate = dfdate.format(new Date());
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date save = null;
		try {
			save = df.parse(savedate);
//			save = df.parse(eventcheckModel.eventTime);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		EventCheck eventcheck = new EventCheck();
		User user = (User)session.getAttribute("user");
		eventcheck.setEmail(user);
		eventcheck.setEventName(eventcheckModel.eventName);
		eventcheck.setEventTime(save);
		ecdao.saveEventCheck(eventcheck);
		return "redirect:/insomniaclub";
	}
	public static class EventCheckModel {
		private String systemTime;
		private String beforeTime;
		private String afterTime;
		private String eventTime;
		private String eventName;
		public void setEventName(String eventName){
			this.eventName = eventName;
		}
		public void setEventTime(String eventTime){
			this.eventTime = eventTime;
		}
		public void setBeforeTime(String beforeTime){
			this.beforeTime = beforeTime;
		}
		public void setAfterTime(String afterTime){
			this.afterTime = afterTime;
		}
		public void setSystemTime(String systemTime){
			this.systemTime = systemTime;
		}
		public String getEventName() {
			return eventName;
		}
		public String getEventTime() {
			return eventTime;
		}
		public String getSystemTime(){
			return systemTime;
		}
		public String getBeforeTime(){
			return beforeTime;
		}
		public String getAfterTime(){
			return afterTime;
		}
	}
}