package insomniaclub.timetable.controller;

import insomniaclub.authentication.model.User;
import insomniaclub.authentication.service.IUserDao;
import insomniaclub.timetable.model.SurveyForm;
import insomniaclub.timetable.model.TimeTable;
import insomniaclub.timetable.service.ISurveyFormDao;
import insomniaclub.timetable.service.ITimeTableDao;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@Scope("session")
@RequestMapping("/insomniaclub/")

public class SuggestTimeTableController {
	@Autowired
	private ISurveyFormDao sfdao;
	@Autowired
	private IUserDao udao;
	@Autowired
	private ITimeTableDao ttdao;

	@RequestMapping(value = "suggesttimetable")
	public String suggesttimetable(HttpSession session, Model model){
		User user;
		SurveyForm surveyform;

		if ( (user = (User)session.getAttribute("user")) == null)
		{
			return "redirect:/insomniaclub";
		}
		else{
			SuggestTimeTableModel suggestTimetableModel = new SuggestTimeTableModel();
			suggestTimetableModel.email = user.getEmail();
			surveyform = sfdao.findByEmail(user).get(0);

			DateFormat df = new SimpleDateFormat("HH:mm:ss");
			Calendar c = Calendar.getInstance();
			Calendar b = Calendar.getInstance();
			Date gtime = null;
			Date btime = null;
			Date judgebtimelast = null;
			Date judgebtimefirst = null;
			Date judgebtimemid = null;
			try {
				gtime = df.parse(surveyform.getGTime());
				btime = df.parse(surveyform.getBTime());
				judgebtimelast = df.parse("21:00:00");
				judgebtimefirst = df.parse("16:00:00");
				judgebtimemid = df.parse("18:00:00");
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			c.setTime(gtime);
			c.add(Calendar.HOUR, -1);
			b.setTime(btime);
			String backtime = null;
			if(btime.before(judgebtimefirst)||btime.equals(judgebtimefirst)){
				backtime = "22:00:00";
			}
			else if(btime.after(judgebtimefirst)&&btime.before(judgebtimemid)||btime.equals(judgebtimemid)){
				b.add(Calendar.HOUR, 4);
				backtime = df.format(b.getTime());
			}
			else if(btime.after(judgebtimemid)&&btime.before(judgebtimelast)||btime.equals(judgebtimelast)){
				b.add(Calendar.HOUR, 3);
				backtime = df.format(b.getTime());
			}
			else if(btime.after(judgebtimelast)){
				b.add(Calendar.HOUR, 1);
				backtime = df.format(b.getTime());
			}

			String gotime = df.format(c.getTime());

			suggestTimetableModel.getUp = "getup";
			suggestTimetableModel.getUpTime =gotime;

			suggestTimetableModel.sleep = "sleep";
			suggestTimetableModel.sleepTime = backtime;
			model.addAttribute("suggesttimetable", suggestTimetableModel);

			return "suggesttimetable";
		}
	}
	@RequestMapping(value = "suggesttimetable", method = RequestMethod.POST)
	public String suggesttimetableChange(@ModelAttribute("suggesttimetablemodel")SuggestTimeTableModel suggesttimetableModel,
				HttpSession session){
		User user;
		if ((user = (User) session.getAttribute("user")) == null)
			return "redirect:/insomniaclub";

		if(session.getAttribute("suggestimetable") != null)
			return "redirect:/insomniaclub/timetable";
		if(suggesttimetableModel.eventName==""||suggesttimetableModel.eventTime==""||suggesttimetableModel.eventName==null||suggesttimetableModel.eventTime==null){
			return "redirect:/insomniaclub/signup";
		}
		else{

			ArrayList<String> name = new ArrayList<String>();
			ArrayList<String> time = new ArrayList<String>();

			List<TimeTable> timetableuser = ttdao.findByEmail(user);
			TimeTable timetable = null;
			for (TimeTable t : timetableuser) {
				ttdao.deleteTimeTable(t);
			}
			String[] Split = suggesttimetableModel.eventName.split(",");
			String[] Splittime = suggesttimetableModel.eventTime.split(",");
			int num = Split.length;
			for(int i = 0; i<Split.length; i++){
				name.add(Split[i]);
			}
			for(int i = 0; i<Splittime.length; i++){
				time.add(Splittime[i]);
			}
			for(int i = 0; i<num; i++){
				timetable = new TimeTable();
				timetable.setEmail(user);
				timetable.setEventName(name.get(i));
				timetable.setEvenTime(time.get(i));

				ttdao.saveTimeTable(timetable);
			}

		return "redirect:/insomniaclub/timetable";
		}



	}

/****/
/**get value from form**/
	@SuppressWarnings("unused")
	private static class SuggestTimeTableModel {
		private String email;
		private String getUp;
		private String getUpTime;
		private String sleep;
		private String sleepTime;


		private String eventName;
		private String eventTime;
		public void setEventTime(String eventTime){
			this.eventTime = eventTime;
		}
		public void setEventName(String eventName){
			this.eventName = eventName;
		}
		public String getEventTime(){
			return eventTime;
		}
		public String getEventName(){

			return eventName;
		}


		public void setEmail(String email){
			this.email = email;
		}

		public void setGetUp(String getUp){
			this.getUp = getUp;
		}

		public void setSleep(String sleep){
			this.sleep = sleep;
		}

		public void setSleepTime(String sleepTime){
			this.sleepTime = sleepTime;
		}

		public void setGetUpTime(String getUpTime){
			this.getUpTime = getUpTime;
		}

		public String getEmail(){
			return email;
		}

		public String getGetUp(){
			return getUp;
		}

		public String getGetUpTime(){
			return getUpTime;
		}

		public String getSleep() {
			return sleep;
		}

		public String getSleepTime(){
			return sleepTime;
		}

	}
}


