package insomniaclub.authentication.model;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

@Entity
@Table(name="user")
public class User {

	@Id
	@Column(name="email")
	@NotNull
	private String email;

	@Column(name="password")
	private String password;

	@Column(name="first_name")
	@NotNull
	private String firstName;

	@Column(name="last_name")
	@NotNull
	private String lastName;

	@Column(name="age")
	private int age;

	@Column(name="gender")
	private String gender;

	@Column(name="date")
	@Temporal(value=TemporalType.TIMESTAMP )
	@Generated(value=GenerationTime.INSERT)
	private Date date;

	@SuppressWarnings("unused")
	private User() {}

	public User(String email) {
		this.email = email;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean checkPassword(String password) {
		return getMD5(password).equals(this.password);
	}

	public void setPassword(String password) {
		this.password = getMD5(password);
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Date getDate() {
		return date;
	}

	private String getMD5(String password) {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] messageDigest = md.digest(password.getBytes());
			BigInteger number = new BigInteger(1, messageDigest);
			String hashtext = number.toString(16);
			while (hashtext.length() < 32) {
				hashtext = "0" + hashtext;
			}
			return hashtext;
		}
		catch (NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof User))
			return false;
		return email.equals(((User) object).email);
	}

}
