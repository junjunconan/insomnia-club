package insomniaclub.authentication.service;

import insomniaclub.authentication.model.User;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;

@Transactional
public class UserDao implements IUserDao {

	@Autowired
	private HibernateTemplate hibernateTemplate;

	@Override
	public void saveUser(User user) {
		hibernateTemplate.save(user);
	}

	@Override
	public void updateUser(User user) {
		hibernateTemplate.update(user);
	}

	@Override
	public void deleteUser(User user) {
		hibernateTemplate.delete(user);
	}

	@Override
	public User findByEmail(String email) {
		return hibernateTemplate.get(User.class, email);
	}

}
