package insomniaclub.authentication.service;

import insomniaclub.authentication.model.User;

public interface IUserDao {

	public void saveUser(User user);

	public void updateUser(User user);

	public void deleteUser(User user);

	public User findByEmail(String email);

}
