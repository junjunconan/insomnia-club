package insomniaclub.authentication.controller;

import insomniaclub.authentication.model.User;
import insomniaclub.authentication.service.IUserDao;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
@Scope("session")
@RequestMapping("/insomniaclub/")
public class SignupController {

	@Autowired
	private IUserDao udao;

	@RequestMapping(value = "signup")
	public String signup(HttpSession session) {
		if (session.getAttribute("user") != null)
			return "redirect:/insomniaclub";
		return "signup";
	}

	@RequestMapping(value = "signup", method = RequestMethod.POST)
	public String signupAction(@ModelAttribute("usermodel") UserModel userModel,
			HttpSession session) {
		if (session.getAttribute("user") != null)
			return "redirect:/insomniaclub";

		if (userModel.password == null || userModel.confirmPassword == null ||
				!userModel.password.equals(userModel.confirmPassword)) {
			return "redirect:/insomniaclub/signup";
		}

		User user = new User(userModel.email);
		user.setPassword(userModel.password);
		user.setFirstName(userModel.firstName);
		user.setLastName(userModel.lastName);

		udao.saveUser(user);

		return "redirect:/insomniaclub";
	}

	@RequestMapping("verify")
	@ResponseBody
	public boolean verifyEmail(@RequestParam(value="email") String email) {
		User user = udao.findByEmail(email);
		if(user != null)
			return true;
		else
			return false;
	}

	@SuppressWarnings("unused")
	private static class UserModel {

		private String email;
		private String password;
		private String confirmPassword;
		private String firstName;
		private String lastName;

		public void setEmail(String email) {
			this.email = email;
		}

		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}

		public void setLastName(String lastName) {
			this.lastName = lastName;
		}

		public void setPassword(String password) {
			this.password = password;
		}

		public void setConfirmPassword(String confirmPassword) {
			this.confirmPassword = confirmPassword;
		}

	}

}
