package insomniaclub.authentication.controller;

import insomniaclub.authentication.model.User;
import insomniaclub.authentication.service.IUserDao;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@Controller
@Scope("session")
@RequestMapping("/insomniaclub/")
public class ProfileController {

	@Autowired
	private IUserDao udao;

	@RequestMapping(value = "profile")
	public String profile(HttpSession session, Model model) {
		User user;
		if ((user = (User) session.getAttribute("user")) == null)
			return "redirect:/insomniaclub";

		ProfileModel profileModel = new ProfileModel();
		profileModel.email = user.getEmail();
		profileModel.firstName = user.getFirstName();
		profileModel.lastName = user.getLastName();
		model.addAttribute("user", profileModel);
		return "profile";
	}

	@RequestMapping(value = "profile", method = RequestMethod.POST)
	public String profileChange(@ModelAttribute("profilemodel")ProfileModel profileModel,
			HttpSession session) {
		User user;
		if ((user = (User) session.getAttribute("user")) == null)
			return "redirect:/insomniaclub";
		user = udao.findByEmail(user.getEmail());

		if (profileModel.password == null || profileModel.confirmPassword == null ||
				!profileModel.password.equals(profileModel.confirmPassword)) {
			return "redirect:/insomniaclub/profile";
		}

		user.setFirstName(profileModel.firstName);
		user.setLastName(profileModel.lastName);
		user.setPassword(profileModel.password);
		udao.updateUser(user);
		session.setAttribute("user", user);

		return "redirect:/insomniaclub";
	}

	@SuppressWarnings("unused")
	private static class ProfileModel {

		private String email;
		private String firstName;
		private String lastName;
		private String password;
		private String confirmPassword;

		public String getEmail() {
			return email;
		}

		public void setEmail(String email) {
			this.email = email;
		}

		public String getFirstName() {
			return firstName;
		}

		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}

		public String getLastName() {
			return lastName;
		}

		public void setLastName(String lastName) {
			this.lastName = lastName;
		}

		public void setPassword(String password) {
			this.password = password;
		}

		public void setConfirmPassword(String confirmPassword) {
			this.confirmPassword = confirmPassword;
		}

	}

}
