package insomniaclub.authentication.controller;

import insomniaclub.authentication.model.User;
import insomniaclub.authentication.service.IUserDao;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@Scope("session")
@RequestMapping("/insomniaclub/")
public class FacebookSigninController {

	@Autowired
	private IUserDao udao;

	@RequestMapping(value = "facebook")
	public String signinWithFacebook(HttpSession session) {
		return "redirect:/insomniaclub";
	}

	@RequestMapping(value = "facebook", method = RequestMethod.POST)
	public String signinWithFacebookAction(@ModelAttribute("usermodel")
			UserModel userModel, HttpSession session) {
		if (session.getAttribute("user") != null)
			return "redirect:/insomniaclub";

		User user = udao.findByEmail(userModel.email);
		if (user == null) {
			user = new User(userModel.email);
			user.setFirstName(userModel.firstName);
			user.setLastName(userModel.lastName);
			udao.saveUser(user);
		}
		session.setAttribute("user", user);
		return "redirect:/insomniaclub";
	}

	@SuppressWarnings("unused")
	private static class UserModel {

		private String email;
		private String firstName;
		private String lastName;

		public void setEmail(String email) {
			this.email = email;
		}

		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}

		public void setLastName(String lastName) {
			this.lastName = lastName;
		}

	}

}
