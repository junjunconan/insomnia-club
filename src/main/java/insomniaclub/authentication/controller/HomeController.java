package insomniaclub.authentication.controller;

import insomniaclub.authentication.model.User;
import insomniaclub.authentication.service.IUserDao;
import insomniaclub.suggestion.model.Suggestion;
import insomniaclub.suggestion.service.ISuggestionRatingDao;
import insomniaclub.timetable.model.EventCheck;
import insomniaclub.timetable.model.SurveyForm;
import insomniaclub.timetable.service.IEventCheckDao;
import insomniaclub.timetable.service.ISurveyFormDao;
import insomniaclub.video.model.Video;
import insomniaclub.video.service.IVideoRateDao;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.fasterxml.jackson.databind.ObjectMapper;


@Controller
@Scope("session")
public class HomeController {

	@Autowired
	private IUserDao udao;
	@Autowired
	private ISuggestionRatingDao srdao;
	@Autowired
	private IVideoRateDao vrDao;
	@Autowired
	private ISurveyFormDao sfdao;
	@Autowired
	private IEventCheckDao evdao;


	@SuppressWarnings({ "rawtypes" })
	@RequestMapping(value = "/insomniaclub")
	public String home(HttpSession session, Model model){
		User user;
		@SuppressWarnings("unused")
		EventCheck eventcheck;
		if((user=(User)session.getAttribute("user")) == null)
			return"index";

		/* Top 10 suggestions */
		DecimalFormat df = new DecimalFormat("0.0");
		List orderedSuggestions = srdao.orderByRating();
		List<SuggestionModel> suggestions = new ArrayList<SuggestionModel>();
		List<Integer> suggestionList = new ArrayList<Integer>();
		for (Object object : orderedSuggestions) {
			Object[] orderedSuggestion = (Object[]) object;
			SuggestionModel suggestion = new SuggestionModel();
			suggestion.suggestion = (Suggestion) orderedSuggestion[0];
			suggestion.avgRating = df.format((double) orderedSuggestion[1]);
			suggestions.add(suggestion);
			suggestionList.add(suggestion.getSuggestion().getId());
		}
		ObjectMapper mapper = new ObjectMapper();
		String json="";
		try {
			json = mapper.writeValueAsString(suggestionList);
		} catch (Exception e) {
			e.printStackTrace();
		}
		model.addAttribute("suggestions", suggestions);
		model.addAttribute("suggestionlist", json);

		/* Top 5 suggestions */
		List orderedVideos = vrDao.orderByRate();
		List<VideoModel> videos = new ArrayList<VideoModel>();
		List<Integer> videoList = new ArrayList<Integer>();
		for (Object object : orderedVideos) {
			Object[] orderedVideo = (Object[]) object;
			VideoModel video = new VideoModel();
			video.video = (Video) orderedVideo[0];
			video.avgRating = df.format((double) orderedVideo[1]);
			videos.add(video);
			videoList.add(video.getVideo().getVideoid());
		}
		mapper = new ObjectMapper();
		String json2="";
		try {
			json2 = mapper.writeValueAsString(videoList);
		} catch (Exception e) {
			e.printStackTrace();
		}
		model.addAttribute("videos", videos);
		model.addAttribute("videolist", json2);

		/** SurveyForm **/
		List<SurveyForm> surveyform = sfdao.findAll();
		List<String> liststudy = new ArrayList<String>();
		List<String> listwork = new ArrayList<String>();
		List<String> listretire = new ArrayList<String>();
		for(SurveyForm s : surveyform){
			if(s.getJob().equalsIgnoreCase("study")){
				liststudy.add(s.getJob());
			}
			else if(s.getJob().equalsIgnoreCase("work")){
				listwork.add(s.getJob());
			}
			else{
				listretire.add(s.getJob());
			}
		}
		model.addAttribute("googleAPI", surveyform);
		int studysize;
		int worksize;
		int retiresize;
		studysize = liststudy.size();
		worksize = listwork.size();
		retiresize = listretire.size();
		model.addAttribute("study",studysize);
		model.addAttribute("work",worksize);
		model.addAttribute("retire",retiresize);

		/**getup sleep on time times**/
		List<EventCheck> eventcheckList = evdao.findByEmail(user);
		List<String> listgetup = new ArrayList<String>();
		List<String> listsleep = new ArrayList<String>();
		for(EventCheck e: eventcheckList){
			if(e.getEventName().equalsIgnoreCase("getup")){
				listgetup.add(e.getEventName());
			}
			else if(e.getEventName().equalsIgnoreCase("sleep")){
				listsleep.add(e.getEventName());
			}
		}
		model.addAttribute("history", eventcheckList);
		int getup;
		int sleep;
		getup = listgetup.size();
		sleep = listsleep.size();
		model.addAttribute("getup", getup);
		model.addAttribute("sleep", sleep);

		/**each month on time check**/
		SimpleDateFormat dfdate = new SimpleDateFormat("MM");
		int eventmonth = 0;
		List<String> JAN = new ArrayList<String>();
		List<String> FEB = new ArrayList<String>();
		List<String> MAR = new ArrayList<String>();
		List<String> APR = new ArrayList<String>();
		List<String> MAY = new ArrayList<String>();
		List<String> JUN = new ArrayList<String>();
		List<String> JUL = new ArrayList<String>();
		List<String> AUG = new ArrayList<String>();
		List<String> SEP = new ArrayList<String>();
		List<String> OCE = new ArrayList<String>();
		List<String> NOV = new ArrayList<String>();
		List<String> DEC = new ArrayList<String>();
		for(int i = 0; i<eventcheckList.size(); i++){
			eventmonth =Integer.parseInt(dfdate.format(eventcheckList.get(i).getEventTime()));
			if(eventmonth == 1){
				JAN.add("JAN");
			}
			else if(eventmonth == 2){
				FEB.add("FEB");
			}
			else if(eventmonth == 3){
				MAR.add("MAR");
			}
			else if(eventmonth == 4){
				APR.add("APR");
			}
			else if(eventmonth == 5){
				MAY.add("MAY");
			}
			else if(eventmonth == 6){
				JUN.add("JUN");
			}
			else if(eventmonth == 7){
				JUL.add("JUL");
			}
			else if(eventmonth == 8){
				AUG.add("AUG");
			}
			else if(eventmonth == 9){
				SEP.add("SEP");
			}
			else if(eventmonth == 10){
				OCE.add("OCE");
			}
			else if(eventmonth == 11){
				NOV.add("NOV");
			}
			else if(eventmonth == 12){
				DEC.add("DEC");
			}
		}
		model.addAttribute("month", eventcheckList);
		int jan,feb,mar,apr,may,jun,jul,aug,sep,oce,nov,dec;
		jan = JAN.size();
		feb = FEB.size();
		mar = MAR.size();
		apr = APR.size();
		may = MAY.size();
		jun = JUN.size();
		jul = JUL.size();
		aug = AUG.size();
		sep = SEP.size();
		oce = OCE.size();
		nov = NOV.size();
		dec = DEC.size();
		model.addAttribute("jan", jan);
		model.addAttribute("feb", feb);
		model.addAttribute("mar", mar);
		model.addAttribute("apr", apr);
		model.addAttribute("may", may);
		model.addAttribute("jun", jun);
		model.addAttribute("jul", jul);
		model.addAttribute("aug", aug);
		model.addAttribute("sep", sep);
		model.addAttribute("oce", oce);
		model.addAttribute("nov", nov);
		model.addAttribute("dec", dec);
		return "home";
	}

	@RequestMapping(value = "/insomniaclub", method = RequestMethod.POST)
	public String loginAction(@ModelAttribute("loginmodel") LoginModel loginModel,
			BindingResult result, RedirectAttributes redirectAttrs, HttpSession session) {
		if (session.getAttribute("user") != null)
			return "redirect:/insomniaclub";

		User user = udao.findByEmail(loginModel.email);

		if (user == null) {
			redirectAttrs.addFlashAttribute("loginerror", "Cannot find your account.");
			return "redirect:/insomniaclub";
		}

		if (!user.checkPassword(loginModel.password)) {
			redirectAttrs.addFlashAttribute("loginerror", "Wrong password.");
			return "redirect:/insomniaclub";
		}

		session.setAttribute("user", user);

		return "redirect:/insomniaclub";
	}


	@RequestMapping(value = "/insomniaclub/logout")
	public String logout(HttpSession session) {
		session.removeAttribute("user");
		return "redirect:/insomniaclub";
	}


	@SuppressWarnings("unused")
	private static class LoginModel {

		private String email;
		private String password;

		public void setEmail(String email) {
			this.email = email;
		}

		public void setPassword(String password) {
			this.password = password;
		}

	}

	@SuppressWarnings("unused")
	private static class SuggestionModel {

		private Suggestion suggestion;
		private String avgRating;

		public Suggestion getSuggestion() {
			return suggestion;
		}

		public String getAvgRating() {
			return avgRating;
		}

	}

	@SuppressWarnings("unused")
	private static class VideoModel {

		private Video video;
		private String avgRating;

		public Video getVideo() {
			return video;
		}

		public String getAvgRating() {
			return avgRating;
		}

	}

}
