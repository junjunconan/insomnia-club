package insomniaclub;

import insomniaclub.authentication.model.User;
import insomniaclub.authentication.service.IUserDao;
import insomniaclub.authentication.service.UserDao;
import insomniaclub.music.model.Music;
import insomniaclub.music.service.IMusicDao;
import insomniaclub.music.service.MusicDao;
import insomniaclub.suggestion.model.Suggestion;
import insomniaclub.suggestion.model.SuggestionComment;
import insomniaclub.suggestion.model.SuggestionRating;
import insomniaclub.suggestion.service.ISuggestionCommentDao;
import insomniaclub.suggestion.service.ISuggestionDao;
import insomniaclub.suggestion.service.ISuggestionRatingDao;
import insomniaclub.suggestion.service.SuggestionCommentDao;
import insomniaclub.suggestion.service.SuggestionDao;
import insomniaclub.suggestion.service.SuggestionRatingDao;
import insomniaclub.timetable.model.EventCheck;
import insomniaclub.timetable.model.SurveyForm;
import insomniaclub.timetable.model.TimeTable;
import insomniaclub.timetable.service.EventCheckDao;
import insomniaclub.timetable.service.IEventCheckDao;
import insomniaclub.timetable.service.ISurveyFormDao;
import insomniaclub.timetable.service.ITimeTableDao;
import insomniaclub.timetable.service.SurveyFormDao;
import insomniaclub.timetable.service.TimeTableDao;
import insomniaclub.video.model.Video;
import insomniaclub.video.model.VideoComment;
import insomniaclub.video.model.VideoRate;
import insomniaclub.video.service.IVideoCommentDao;
import insomniaclub.video.service.IVideoDao;
import insomniaclub.video.service.IVideoRateDao;
import insomniaclub.video.service.VideoCommentDao;
import insomniaclub.video.service.VideoDao;
import insomniaclub.video.service.VideoRateDao;

import javax.servlet.MultipartConfigElement;
import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;
import org.hibernate.SessionFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.embedded.MultipartConfigFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.Primary;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBuilder;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@ComponentScan(excludeFilters = @ComponentScan.Filter(
		type = FilterType.ANNOTATION,
		value = Application.Exclude.class
		))
@EnableAutoConfiguration
@EnableTransactionManagement
public class Application {

	@Bean
	public IUserDao userDao() {
		return new UserDao();
	}

	@Bean
	public ISuggestionDao suggestionDao() {
		return new SuggestionDao();
	}

	@Bean
	public ISuggestionRatingDao suggestionRatingDao() {
		return new SuggestionRatingDao();
	}

	@Bean
	public ISuggestionCommentDao suggestionCommentDao() {
		return new SuggestionCommentDao();
	}

	@Bean
	public ITimeTableDao timeTableDao() {
		return new TimeTableDao();
	}
	
	@Bean
	public IEventCheckDao eventCheckDao() {
		return new EventCheckDao();
	}
	
	@Bean
	public ISurveyFormDao surveyFormDao() {
		return new SurveyFormDao();
	}

	@Bean
	public IMusicDao musicDao() {
		return new MusicDao();
	}
	
	@Bean
	public IVideoDao videoDao() {
		return new VideoDao();
	}
	
	@Bean
	public IVideoCommentDao videocommentDao() {
		return new VideoCommentDao();
	}
	
	@Bean
	public IVideoRateDao videorateDao() {
		return new VideoRateDao();
	}
	
	

	@Bean
	public HibernateTemplate hibernateTemplate() {
		return new HibernateTemplate(sessionFactory());
	}

	@Bean
	public SessionFactory sessionFactory() {
		return new LocalSessionFactoryBuilder(getDataSource())
					.addAnnotatedClasses(User.class, TimeTable.class, SurveyForm.class, EventCheck.class,
							Suggestion.class, SuggestionRating.class, SuggestionComment.class,
							Music.class, Video.class, VideoComment.class, VideoRate.class)
					.buildSessionFactory();
	}

	@Bean
	public DataSource getDataSource() {
		BasicDataSource dataSource = new BasicDataSource();
		dataSource.setDriverClassName("com.mysql.jdbc.Driver");
		dataSource.setUrl("jdbc:mysql://localhost:3306/insomnia");
		dataSource.setUsername("root");
		dataSource.setPassword("123456");

		return dataSource;
	}

	@Bean
	@Primary
	public HibernateTransactionManager hibTransMan(){
		return new HibernateTransactionManager(sessionFactory());
	}
	
	@Bean
	public MultipartConfigElement multipartConfigElement() {
		MultipartConfigFactory factory = new MultipartConfigFactory();
		factory.setMaxFileSize("10240KB");
		factory.setMaxRequestSize("10240KB");
		return factory.createMultipartConfig();
	}

	public @interface Exclude {};

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

}

