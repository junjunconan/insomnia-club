package insomniaclub;

import java.text.SimpleDateFormat;
import java.util.Date;

import insomniaclub.authentication.model.User;
import insomniaclub.authentication.service.IUserDao;
import insomniaclub.music.service.IMusicDao;
import insomniaclub.suggestion.service.ISuggestionCommentDao;
import insomniaclub.suggestion.service.ISuggestionDao;
import insomniaclub.suggestion.service.ISuggestionRatingDao;
import insomniaclub.timetable.service.IEventCheckDao;
import insomniaclub.timetable.service.ISurveyFormDao;
import insomniaclub.timetable.service.ITimeTableDao;
import insomniaclub.video.service.IVideoCommentDao;
import insomniaclub.video.service.IVideoDao;
import insomniaclub.video.service.IVideoRateDao;

import org.junit.After;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.AnnotationConfigWebContextLoader;
import org.springframework.test.context.web.WebAppConfiguration;

import com.sun.el.parser.ParseException;

@RunWith(SpringJUnit4ClassRunner.class)
@Application.Exclude
@WebAppConfiguration
@ContextConfiguration(loader = AnnotationConfigWebContextLoader.class, classes = {Application.class})
public abstract class TestBase {

	protected static final String EMAIL = "abcd@gmail.com";
	protected static final String WRONG_EMAIL = "abc@gmail.com";
	protected static final String PASSWORD = "123456";
	protected static final String WRONG_PASSWORD = "abcdef";
	protected static final String FIRST_NAME = "Wenjun";
	protected static final String LAST_NAME = "Xi";
	protected static final String SUGGESTION_TITLE = "Coding!";
	protected static final String SUGGESTION_DES = "Coding makes you sleepy!";
	protected static final int SUGGESTION_RATING = 5;
	protected static final String SUGGESTION_COMMENT = "Good!";
	
	protected static final int AGE = 24;
	protected static final String GENDER="m";
	protected static final String JOB = "study";
	protected static final String G_TIME = "07:00";
	protected static final String B_TIME = "18:00";
	protected static final String TYPE_MUSIC = "pop";
	protected static final boolean A_EXERCISE = true;
	protected static final String EXERCISE = "true";
	protected static final boolean A_EMAIL = true;
	protected static final String aEMAIL = "true";
	
	protected static final String EVENT = "lunch";
	protected static final String EVEN_TIME = "12:00";
	protected static final Date EVENT_DATE = new Date(Long.parseLong("1412740177488"));
	protected static final String EVEN_DATE = "20-10-2014";
	
	protected static final String MUSIC_NAME = "Some music";
	protected static final String MUSIC_SINGER = "Singer";
	protected static final String MUSIC_TYPE = "Country";
	protected static final String MUSIC_KEYWORD1 = "1";
	protected static final String MUSIC_KEYWORD2 = "2";
	protected static final String MUSIC_KEYWORD3 = "3";

	
	protected static final String CATEGORY ="intro";
	protected static final String VIDEO_DESCRIPTION = "Professional is telling you";
	protected static final String VIDEO_PATH = "https://www.youtube.com/embed/_tXFhGRqggc";
	protected static final String VIDEO_TITLE = "what is insomnia";
	protected static final int VIDEO_ID = 1;
	protected static final int VIDEO_RATE = 5;
	protected static final String VIDEO_COMMENT = "Good!";
	
	@Autowired
	protected IUserDao udao;
	@Autowired
	protected ISuggestionDao suggestionDao;
	@Autowired
	protected ISuggestionRatingDao srDao;
	@Autowired
	protected ISuggestionCommentDao scDao;
	
	@Autowired
	protected ISurveyFormDao sfdao;
	@Autowired
	protected ITimeTableDao ittdao;
	@Autowired
	protected IEventCheckDao iecdao;
	
	@Autowired
	protected IMusicDao musicDao;
	
	@Autowired
	protected IVideoDao videoDao;
	
	@Autowired
	protected IVideoCommentDao vcDao;
	
	@Autowired
	protected IVideoRateDao vrDao;
	
	@After
	public void deleteDatabaseEntities() throws Exception {
		User user = udao.findByEmail(EMAIL);
		if (user != null)
			udao.deleteUser(user);
	}

}
