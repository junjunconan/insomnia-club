package insomniaclub.video.controller;

import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.util.List;

import insomniaclub.ControllerTestBase;
import insomniaclub.video.model.Video;
import insomniaclub.video.model.VideoComment;
import insomniaclub.video.model.VideoRate;
import insomniaclub.video.service.IVideoDao;

import org.junit.Before;
import org.junit.Test;

public class VideoControllerTest extends ControllerTestBase {

	@Before
	public void doSignup() throws Exception {
		signup();
	}

	@Test
	public void testPageRedirection() throws Exception {
		mockMvc.perform(get("/insomniaclub/videos"))
			.andExpect(redirectedUrl("/insomniaclub"));

	}
	
	@Test
	public void testRatingAndComment() throws Exception {
		mockMvc.perform(get("/insomniaclub/videos")
				.param("title", VIDEO_TITLE)
				.param("description", VIDEO_DESCRIPTION)
				.sessionAttr("user", user));
		
		List<Video> retrieved = videoDao.findAll();
		boolean found = false;
		Video video = null;
		for (Video v : retrieved) {
			if (v.getVideotitle().equals(VIDEO_TITLE) &&
				v.getDescription().equals(VIDEO_DESCRIPTION)&&
				v.getCategory().equals(CATEGORY)) {
				video = v;
				found = true;
				break;
			}
		}
		assertTrue(found);
	}
}