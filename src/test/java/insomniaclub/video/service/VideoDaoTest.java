package insomniaclub.video.service;

import static org.junit.Assert.assertTrue;
import insomniaclub.TestBase;
import insomniaclub.video.model.Video;

import java.util.List;

import org.junit.Test;

public class VideoDaoTest extends TestBase {

	@Test
	public void testDatabasePersistency() throws Exception {
		Video video = new Video();
		video.setCategory(CATEGORY);
		video.setDescription(VIDEO_DESCRIPTION);
		video.setVideopath(VIDEO_PATH);
		video.setVideotitle(VIDEO_TITLE);
		videoDao.saveVideo(video);

		List<Video> retrieved = videoDao.findAll();
		boolean found = false;
		for (Video v : retrieved) {
			if (v.getCategory().equals(CATEGORY) &&
					v.getDescription().equals(VIDEO_DESCRIPTION) &&
					v.getVideopath().equals(VIDEO_PATH) &&
					v.getVideotitle().equals(VIDEO_TITLE)) {
				found = true;
				break;
			}
		}
		assertTrue(found);

		videoDao.deleteVideo(video);
	}

}
