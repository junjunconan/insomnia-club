package insomniaclub.video.model;
import static org.junit.Assert.assertEquals;
import insomniaclub.TestBase;

import org.junit.Test;



public class VideoTest extends TestBase {

	@Test
	public void testModel() throws Exception {
		Video video = new Video();
		video.setVideoid(VIDEO_ID);
		video.setVideotitle(VIDEO_TITLE);
		video.setCategory(CATEGORY);
		video.setVideopath(VIDEO_PATH);
		video.setVideoid(VIDEO_ID);
		video.setDescription(VIDEO_DESCRIPTION);
		assertEquals(video.getCategory(), CATEGORY);
		assertEquals(video.getDescription(), VIDEO_DESCRIPTION);
		assertEquals(video.getVideopath(), VIDEO_PATH);
		assertEquals(video.getVideotitle(), VIDEO_TITLE);
		assertEquals(video.getVideoid(), VIDEO_ID);
	}

}