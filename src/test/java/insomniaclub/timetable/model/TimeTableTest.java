package insomniaclub.timetable.model;

import static org.junit.Assert.*;
import insomniaclub.TestBase;
import insomniaclub.authentication.model.User;

import org.junit.Test;

public class TimeTableTest extends TestBase{
	
	@Test
	public void testModel() throws Exception {
		User user = new User(EMAIL);
		user.setFirstName(FIRST_NAME);
		user.setLastName(LAST_NAME);
		
		TimeTable timetable = new TimeTable();
		timetable.setEmail(user);
		timetable.setEventName(EVENT);
		timetable.setEvenTime(EVEN_TIME);
		assertEquals(EVENT, timetable.getEventName());
		assertEquals(EVEN_TIME, timetable.getEventTime());
		
	}

}
