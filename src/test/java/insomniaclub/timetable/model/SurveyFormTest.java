package insomniaclub.timetable.model;

import static org.junit.Assert.*;

import org.junit.Test;

import insomniaclub.TestBase;
import insomniaclub.authentication.model.User;

public class SurveyFormTest extends TestBase{
	@Test
	public void testModel() throws Exception {
		User user = new User(EMAIL);
		user.setFirstName(FIRST_NAME);
		user.setLastName(LAST_NAME);
		user.setAge(AGE);
		user.setGender(GENDER);
		SurveyForm surveyform = new SurveyForm();
		surveyform.setEmail(user);
		surveyform.setJob(JOB);
		surveyform.setGTime(G_TIME);
		surveyform.setBTime(B_TIME);
		surveyform.setTypeMusic(TYPE_MUSIC);
		surveyform.setAExercise(A_EXERCISE);
		surveyform.setAEmail(A_EMAIL);
		assertEquals(user, surveyform.getEmail());
		assertEquals(JOB, surveyform.getJob());
		assertEquals(B_TIME, surveyform.getBTime());
		assertEquals(G_TIME, surveyform.getGTime());
		assertEquals(TYPE_MUSIC, surveyform.getTypeMusic());
		assertEquals(A_EXERCISE, surveyform.getAExercise());
		assertEquals(A_EMAIL, surveyform.getAEmail());
	}
}
