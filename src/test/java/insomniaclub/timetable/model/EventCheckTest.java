package insomniaclub.timetable.model;

import static org.junit.Assert.*;

import org.junit.Test;

import insomniaclub.TestBase;
import insomniaclub.authentication.model.User;

public class EventCheckTest extends TestBase {

	@Test
	public void testModel() throws Exception {
		User user = new User(EMAIL);
		user.setFirstName(FIRST_NAME);
		user.setLastName(LAST_NAME);
		
		EventCheck eventcheck = new EventCheck();
		eventcheck.setEmail(user);
		eventcheck.setEventName(EVENT);
		eventcheck.setEventTime(EVENT_DATE);
		assertEquals(EVENT,eventcheck.getEventName());
		assertEquals(EVENT_DATE, eventcheck.getEventTime());
	}
}
