package insomniaclub.timetable.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import insomniaclub.ControllerTestBase;
import insomniaclub.authentication.model.User;
import insomniaclub.timetable.model.SurveyForm;
import static org.junit.Assert.assertTrue;
public class SurveyFormControllerTest extends ControllerTestBase{

	@Before
	public void doSignup() throws Exception {
		signup();
	}
	
	@Test
	public void testPage() throws Exception{	
		mockMvc.perform(get("/insomniaclub/surveyform").sessionAttr("user", user))
			.andExpect(view().name("surveyform"));
	}
	@Test
	public void testSurveyForm() throws Exception{
		mockMvc.perform(post("/insomniaclub/surveyform"))
			.andExpect(redirectedUrl("/insomniaclub"));
		mockMvc.perform(post("/insomniaclub/surveyform")
				.param("job", JOB)
				.param("gtime", G_TIME)
				.param("btime", B_TIME)
				.param("typemusic", TYPE_MUSIC)
				.param("aexercise", EXERCISE)
				.param("aemail", aEMAIL)
				.sessionAttr("user", user))
				.andExpect(redirectedUrl("/insomniaclub/surveyform"));
		
	}
}
