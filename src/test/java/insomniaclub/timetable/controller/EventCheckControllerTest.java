package insomniaclub.timetable.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import insomniaclub.ControllerTestBase;
import insomniaclub.timetable.model.EventCheck;
import insomniaclub.timetable.model.TimeTable;

public class EventCheckControllerTest extends ControllerTestBase{
	@Before
	public void doSignup() throws Exception {
		signup();
	}
	@Test
	public void testPage() throws Exception{
		mockMvc.perform(get("/insomniaclub/eventcheck"))
			.andExpect(redirectedUrl("/insomniaclub"));
		
		mockMvc.perform(get("/insomniaclub/eventcheck").sessionAttr("user", user))
			.andExpect(view().name("redirect:/insomniaclub/surveyform"));
		
	}
	
	@Test
	public void testeventcheck() throws Exception{
		mockMvc.perform(post("/insomniaclub/eventcheck")
				.param("event", EVENT)
				.param("eventdate", EVEN_DATE)
				.sessionAttr("user", user))
				.andExpect(redirectedUrl("/insomniaclub"));
	}
}