package insomniaclub.timetable.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.Before;
import org.junit.Test;

import insomniaclub.ControllerTestBase;

public class SuggestTimeTableControllerTest extends ControllerTestBase{
	@Before
	public void doSignup() throws Exception {
		signup();
	}
	
	@Test
	public void testPage() throws Exception{
		mockMvc.perform(get("/insomniaclub/suggesttimetable"))
			.andExpect(redirectedUrl("/insomniaclub"));
		}
	
	@Test
	public void suggesttimetabletest() throws Exception{
		mockMvc.perform(post("/insomniaclub/suggesttimetable"))
			.andExpect(redirectedUrl("/insomniaclub"));
		mockMvc.perform(post("/insomniaclub/suggesttimetable")
				.param("event", EVENT)
				.param("time", EVEN_TIME)
				.sessionAttr("user", user))
				.andExpect(redirectedUrl("/insomniaclub/signup"));
	}
}