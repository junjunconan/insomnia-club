package insomniaclub.timetable.controller;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import insomniaclub.ControllerTestBase;
import insomniaclub.timetable.model.TimeTable;

public class TimeTableControllerTest extends ControllerTestBase{
	@Before
	public void doSignup() throws Exception {
		signup();
	}
	
	@Test
	public void testPage() throws Exception{
		mockMvc.perform(get("/insomniaclub/timetable"))
			.andExpect(redirectedUrl("/insomniaclub"));
		
		mockMvc.perform(get("/insomniaclub/timetable").sessionAttr("user", user))
		.andExpect(view().name("redirect:/insomniaclub/surveyform"));
		
		if(ittdao.findByEmail(user).size() == 0){
			mockMvc.perform(get("/insomniaclub/timetable"))
			.andExpect(redirectedUrl("/insomniaclub"));
		}
	}
	
	@Test
	public void testdisplayPage() throws Exception{
		if(ittdao.findByEmail(user).size() == 0){
			mockMvc.perform(get("/insomniaclub/timetable"))
			.andExpect(redirectedUrl("/insomniaclub"));
		}
		mockMvc.perform(post("/insomniaclub/surveyform")
			.param("event", EVENT)
			.param("eventtime", EVEN_TIME)
			.sessionAttr("user", user))
			.andExpect(redirectedUrl("/insomniaclub/surveyform"));
		
		List<TimeTable> timetable = ittdao.findByEmail(user);
		List<Integer> list = new ArrayList<Integer>();
		boolean found = false;
		for(TimeTable t : timetable){
			list.add(t.getId());
		}
		
	}
}