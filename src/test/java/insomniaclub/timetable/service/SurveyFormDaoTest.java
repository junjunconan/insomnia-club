package insomniaclub.timetable.service;

import insomniaclub.TestBase;
import insomniaclub.authentication.model.User;
import insomniaclub.timetable.model.SurveyForm;

import org.junit.Test;

public class SurveyFormDaoTest extends TestBase {

@Test
public void testDatabasePersistency() throws Exception {
	User user = new User(EMAIL);
	user.setEmail(EMAIL);
	user.setFirstName(FIRST_NAME);
	user.setLastName(LAST_NAME);
	udao.saveUser(user);
	
	SurveyForm surveyform = new SurveyForm();
	surveyform.setEmail(user);
	surveyform.setJob(JOB);
	surveyform.setGTime(G_TIME);
	surveyform.setBTime(B_TIME);
	surveyform.setTypeMusic(TYPE_MUSIC);
	surveyform.setAExercise(A_EXERCISE);
	surveyform.setAEmail(A_EMAIL);
	sfdao.saveSurveyForm(surveyform);


	/*SurveyForm retrievedSurveyForm = sfdao.findByEmail(EMAIL);
	assertEquals(retrievedSurveyForm, surveyform);
	assertEquals(JOB, retrievedSurveyForm.getJob());
	assertEquals(G_TIME, retrievedSurveyForm.getGTime());
	assertEquals(B_TIME, retrievedSurveyForm.getBTime());
	assertEquals(TYPE_MUSIC, retrievedSurveyForm.getTypeMusic());
	assertEquals(A_EXERCISE, retrievedSurveyForm.getAExercise());
	assertEquals(A_EMAIL, retrievedSurveyForm.getAEmail());*/
 }
}
