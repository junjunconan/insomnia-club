package insomniaclub.timetable.service;

import static org.junit.Assert.assertTrue;
import insomniaclub.TestBase;
import insomniaclub.authentication.model.User;
import insomniaclub.timetable.model.TimeTable;

import java.util.List;

import org.junit.Test;

public class TimeTableDaoTest extends TestBase{

	@Test
	public void testDatabasePersistency() throws Exception {
		User user = new User(EMAIL);
		user.setPassword(PASSWORD);
		user.setFirstName(FIRST_NAME);
		user.setLastName(LAST_NAME);
		udao.saveUser(user);

		TimeTable timetable = new TimeTable(user, EVENT, EVEN_TIME);

		ittdao.saveTimeTable(timetable);

		List<TimeTable> retrieved = ittdao.findAll();
		boolean found = false;
		for (TimeTable t : retrieved) {
			if(t.getEmail().equals(user)&&
					t.getEventName().equals(EVENT)&&
					t.getEventTime().startsWith(EVEN_TIME)){
				found = true;
				break;
			}
		}
		assertTrue(found);
	}
}
