package insomniaclub.timetable.service;

import static org.junit.Assert.assertTrue;
import insomniaclub.TestBase;
import insomniaclub.authentication.model.User;
import insomniaclub.timetable.model.EventCheck;

import java.util.List;

import org.junit.Test;

public class EventCheckDaoTest extends TestBase {

	@Test
	public void testDatabasePersistency() throws Exception {
		User user = new User(EMAIL);
		user.setPassword(PASSWORD);
		user.setFirstName(FIRST_NAME);
		user.setLastName(LAST_NAME);
		udao.saveUser(user);
		EventCheck eventcheck = new EventCheck(user, EVENT, EVENT_DATE);

		iecdao.saveEventCheck(eventcheck);

		List<EventCheck> retrieved = iecdao.findAll();
		boolean found = false;
		for (EventCheck e : retrieved) {
			if(e.getEmail().equals(user)&&
				e.getEventName().equals(EVENT)){
				found = true;
				break;
			}
		}
		assertTrue(found);

	}
}
