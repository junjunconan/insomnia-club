package insomniaclub;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import insomniaclub.authentication.model.User;

import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

public abstract class ControllerTestBase extends TestBase {

	protected User user;
	protected MockMvc mockMvc;

	@Autowired
	private WebApplicationContext wac;

	@Before
	public void setup() {
		mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
	}

	protected void signup() throws Exception {
		mockMvc.perform(post("/insomniaclub/signup").param("email", EMAIL)
				.param("firstName", FIRST_NAME)
				.param("lastName", LAST_NAME)
				.param("password", PASSWORD)
				.param("confirmPassword", PASSWORD))
				.andExpect(redirectedUrl("/insomniaclub"));
		user = udao.findByEmail(EMAIL);
	}

}
