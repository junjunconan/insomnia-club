package insomniaclub.authentication.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import insomniaclub.TestBase;

import org.junit.Test;

public class UserTest extends TestBase {

	@Test
	public void testModel() throws Exception {
		User user = new User(EMAIL);
		user.setFirstName(FIRST_NAME);
		user.setLastName(LAST_NAME);
		assertEquals(EMAIL, user.getEmail());
		assertEquals(FIRST_NAME, user.getFirstName());
		assertEquals(LAST_NAME, user.getLastName());
	}

	@Test
	public void testPasswordValidation() throws Exception {
		User user = new User(EMAIL);
		user.setPassword(PASSWORD);
		assertTrue(user.checkPassword(PASSWORD));
		assertFalse(user.checkPassword(WRONG_PASSWORD));
	}

	@Test
	public void testEquality() {
		User user = new User(EMAIL);
		assertNotEquals(user, new Object());
		assertNotEquals(user, null);
	}

}
