package insomniaclub.authentication.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import insomniaclub.TestBase;
import insomniaclub.authentication.model.User;

import org.junit.Test;

public class UserDaoTest extends TestBase {

	@Test
	public void testDatabasePersistency() throws Exception {
		User user = new User(EMAIL);
		user.setPassword(PASSWORD);
		user.setFirstName(FIRST_NAME);
		user.setLastName(LAST_NAME);
		udao.saveUser(user);
		User retrievedUser = udao.findByEmail(EMAIL);
		assertEquals(retrievedUser, user);
		assertEquals(EMAIL, retrievedUser.getEmail());
		assertEquals(FIRST_NAME, retrievedUser.getFirstName());
		assertEquals(LAST_NAME, retrievedUser.getLastName());
		assertTrue(retrievedUser.checkPassword(PASSWORD));
	}

}
