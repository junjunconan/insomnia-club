package insomniaclub.authentication.controller;

import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.request;
import insomniaclub.ControllerTestBase;
import insomniaclub.authentication.model.User;

import org.junit.Test;

public class FacebookSigninControllerTest extends ControllerTestBase {

	@Test
	public void testFacebookSigninPageRedirection() throws Exception {
		mockMvc.perform(get("/insomniaclub/facebook"))
				.andExpect(redirectedUrl("/insomniaclub"));
	}

	@Test
	public void testSuccessfullyFacebookSignin() throws Exception {
		mockMvc.perform(post("/insomniaclub/facebook")
				.param("email", EMAIL)
				.param("firstName", FIRST_NAME)
				.param("lastName", LAST_NAME))
				.andExpect(request().sessionAttribute("user", notNullValue()))
				.andExpect(redirectedUrl("/insomniaclub"));

		User user = udao.findByEmail(EMAIL);
		assertEquals(EMAIL, user.getEmail());
		assertEquals(FIRST_NAME, user.getFirstName());
		assertEquals(LAST_NAME, user.getLastName());
	}

}
