package insomniaclub.authentication.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import insomniaclub.ControllerTestBase;
import insomniaclub.authentication.model.User;

import org.junit.Test;

public class SignupControllerTest extends ControllerTestBase {

	@Test
	public void testSignupPage() throws Exception {
		mockMvc.perform(get("/insomniaclub/signup"))
				.andExpect(view().name("signup"));
		mockMvc.perform(get("/insomniaclub/signup").sessionAttr("user", new User("")))
				.andExpect(redirectedUrl("/insomniaclub"));
	}

	@Test
	public void testSuccessfullySignup() throws Exception {
		mockMvc.perform(post("/insomniaclub/signup").param("email", EMAIL)
				.param("firstName", FIRST_NAME)
				.param("lastName", LAST_NAME)
				.param("password", PASSWORD)
				.param("confirmPassword", PASSWORD))
				.andExpect(redirectedUrl("/insomniaclub"));

		User user = udao.findByEmail(EMAIL);
		assertEquals(EMAIL, user.getEmail());
		assertEquals(FIRST_NAME, user.getFirstName());
		assertEquals(LAST_NAME, user.getLastName());
		assertTrue(user.checkPassword(PASSWORD));
	}

	@Test
	public void testSignupVerifyEmail() throws Exception {
		mockMvc.perform(get("/insomniaclub/verify").param("email", EMAIL))
				.andExpect(jsonPath("$").value(false));

		signup();
		mockMvc.perform(get("/insomniaclub/verify").param("email", EMAIL))
				.andExpect(jsonPath("$").value(true));
	}

}
