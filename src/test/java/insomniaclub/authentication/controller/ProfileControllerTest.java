package insomniaclub.authentication.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import insomniaclub.ControllerTestBase;

import org.junit.Before;
import org.junit.Test;

public class ProfileControllerTest extends ControllerTestBase {

	@Before
	public void doSignup() throws Exception {
		signup();
	}

	@Test
	public void testPageRedirection() throws Exception {
		mockMvc.perform(get("/insomniaclub/profile"))
			.andExpect(redirectedUrl("/insomniaclub"));

		mockMvc.perform(get("/insomniaclub/profile").sessionAttr("user", user))
			.andExpect(view().name("profile"))
			.andExpect(model().attributeExists("user"));
	}

	@Test
	public void testProfileChange() throws Exception {
		mockMvc.perform(post("/insomniaclub/profile"))
				.andExpect(redirectedUrl("/insomniaclub"));

		mockMvc.perform(post("/insomniaclub/profile").param("email", EMAIL)
				.param("firstName", FIRST_NAME)
				.param("lastName", LAST_NAME)
				.param("password", WRONG_PASSWORD)
				.param("confirmPassword", WRONG_PASSWORD)
				.sessionAttr("user", user))
				.andExpect(redirectedUrl("/insomniaclub"));

		user = udao.findByEmail(EMAIL);
		assertEquals(EMAIL, user.getEmail());
		assertEquals(FIRST_NAME, user.getFirstName());
		assertEquals(LAST_NAME, user.getLastName());
		assertTrue(user.checkPassword(WRONG_PASSWORD));
	}

}
