package insomniaclub.authentication.controller;

import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.flash;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.request;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import insomniaclub.ControllerTestBase;
import insomniaclub.authentication.model.User;

import org.junit.Before;
import org.junit.Test;

public class HomeControllerTest extends ControllerTestBase {

	@Before
	public void doSignup() throws Exception {
		signup();
	}

	@Test
	public void testIndexPage() throws Exception {
		mockMvc.perform(get("/insomniaclub"))
				.andDo(print())
				.andExpect(view().name("index"));
	}

	@Test
	public void testHomePage() throws Exception {
		mockMvc.perform(get("/insomniaclub").sessionAttr("user", new User("")))
				.andExpect(view().name("home"))
				.andExpect(model().attributeExists("suggestions"))
				.andExpect(model().attributeExists("suggestionlist"))
				.andExpect(model().attributeExists("videos"))
				.andExpect(model().attributeExists("videolist"))
				.andExpect(model().attributeExists("googleAPI"))
				.andExpect(model().attributeExists("study"))
				.andExpect(model().attributeExists("work"))
				.andExpect(model().attributeExists("retire"))
				.andExpect(model().attributeExists("getup"))
				.andExpect(model().attributeExists("sleep"));
	}

	@Test
	public void testSuccessfullyLogin() throws Exception {
		mockMvc.perform(post("/insomniaclub").param("email", EMAIL)
				.param("password", PASSWORD))
				.andExpect(request().sessionAttribute("user", notNullValue()))
				.andExpect(redirectedUrl("/insomniaclub"));
	}

	@Test
	public void testLoginWithWrongEmail() throws Exception {
		mockMvc.perform(post("/insomniaclub").param("email", WRONG_EMAIL)
				.param("password", PASSWORD))
				.andExpect(flash().attribute(
						"loginerror", "Cannot find your account."));
	}

	@Test
	public void testLoginWithWrongPassword() throws Exception {
		mockMvc.perform(post("/insomniaclub").param("email", EMAIL)
				.param("password", WRONG_PASSWORD))
				.andExpect(flash().attribute(
						"loginerror", "Wrong password."));
	}

	@Test
	public void testLogout() throws Exception {
		mockMvc.perform(get("/insomniaclub/logout")
				.sessionAttr("user", new User("")))
				.andExpect(request().sessionAttribute("user", nullValue()))
				.andExpect(redirectedUrl("/insomniaclub"));
	}

}
