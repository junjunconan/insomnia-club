package insomniaclub.suggestion.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import insomniaclub.TestBase;
import insomniaclub.authentication.model.User;

import org.junit.Test;

public class SuggestionCommentTest extends TestBase {

	@Test
	public void testModel() throws Exception {
		User user = new User(EMAIL);
		user.setFirstName(FIRST_NAME);
		user.setLastName(LAST_NAME);
		Suggestion suggestion = new Suggestion(SUGGESTION_TITLE, SUGGESTION_DES, user);
		SuggestionComment suggestionComment = new SuggestionComment(SUGGESTION_COMMENT,
													user, suggestion);
		assertEquals(SUGGESTION_COMMENT, suggestionComment.getComment());
		assertEquals(user, suggestionComment.getUser());
		assertEquals(suggestion, suggestionComment.getSuggestion());
	}

	@Test
	public void testEquality() {
		SuggestionComment suggestionComment = new SuggestionComment(SUGGESTION_COMMENT,
												new User(""),
												new Suggestion(SUGGESTION_TITLE,
														SUGGESTION_DES, new User("")));
		assertNotEquals(suggestionComment, new Object());
		assertNotEquals(suggestionComment, null);
	}

}
