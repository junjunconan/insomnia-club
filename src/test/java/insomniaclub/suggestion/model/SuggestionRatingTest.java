package insomniaclub.suggestion.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import insomniaclub.TestBase;
import insomniaclub.authentication.model.User;

import org.junit.Test;

public class SuggestionRatingTest extends TestBase {

	@Test
	public void testModel() throws Exception {
		User user = new User(EMAIL);
		user.setFirstName(FIRST_NAME);
		user.setLastName(LAST_NAME);
		Suggestion suggestion = new Suggestion(SUGGESTION_TITLE, SUGGESTION_DES, user);
		SuggestionRating suggestionRating = new SuggestionRating(SUGGESTION_RATING,
													user, suggestion);
		assertEquals(SUGGESTION_RATING, suggestionRating.getRating());
		assertEquals(user, suggestionRating.getUser());
		assertEquals(suggestion, suggestionRating.getSuggestion());
	}

	@Test
	public void testEquality() {
		SuggestionRating suggestionRating = new SuggestionRating(SUGGESTION_RATING,
												new User(""),
												new Suggestion(SUGGESTION_TITLE,
														SUGGESTION_DES, new User("")));
		assertNotEquals(suggestionRating, new Object());
		assertNotEquals(suggestionRating, null);
	}

}
