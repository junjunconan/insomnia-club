package insomniaclub.suggestion.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import insomniaclub.TestBase;
import insomniaclub.authentication.model.User;

import org.junit.Test;

public class SuggestionTest extends TestBase {

	@Test
	public void testModel() throws Exception {
		User user = new User(EMAIL);
		user.setFirstName(FIRST_NAME);
		user.setLastName(LAST_NAME);
		Suggestion suggestion = new Suggestion(SUGGESTION_TITLE, SUGGESTION_DES, user);
		assertEquals(user, suggestion.getUser());
	}

	@Test
	public void testEquality() {
		Suggestion suggestion = new Suggestion(SUGGESTION_TITLE, SUGGESTION_DES, new User(""));
		assertNotEquals(suggestion, new Object());
		assertNotEquals(suggestion, null);
	}

}
