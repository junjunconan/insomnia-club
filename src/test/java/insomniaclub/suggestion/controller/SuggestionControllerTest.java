package insomniaclub.suggestion.controller;

import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import insomniaclub.ControllerTestBase;
import insomniaclub.suggestion.model.Suggestion;
import insomniaclub.suggestion.model.SuggestionComment;
import insomniaclub.suggestion.model.SuggestionRating;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class SuggestionControllerTest extends ControllerTestBase {

	@Before
	public void doSignup() throws Exception {
		signup();
	}

	@Test
	public void testPageRedirection() throws Exception {
		mockMvc.perform(get("/insomniaclub/suggestion"))
			.andExpect(redirectedUrl("/insomniaclub"));

		mockMvc.perform(get("/insomniaclub/suggestion").sessionAttr("user", user))
			.andExpect(view().name("suggestion"))
			.andExpect(model().attributeExists("suggestions"))
			.andExpect(model().attributeExists("list"));
	}

	@Test
	public void testRatingAndComment() throws Exception {
		mockMvc.perform(post("/insomniaclub/propose")
				.param("title", SUGGESTION_TITLE)
				.param("description", SUGGESTION_DES)
				.sessionAttr("user", user))
				.andExpect(redirectedUrl("/insomniaclub"));

		List<Suggestion> retrieved = suggestionDao.findAll();
		Suggestion suggestion = null;
		boolean found = false;
		for (Suggestion s : retrieved) {
			if (s.getUser().equals(user) &&
				s.getTitle().equals(SUGGESTION_TITLE) &&
				s.getDescription().equals(SUGGESTION_DES)) {
				suggestion = s;
				found = true;
				break;
			}
		}
		assertTrue(found);

		mockMvc.perform(post("/insomniaclub/suggestion/" + suggestion.getId())
				.param("rating", String.valueOf(SUGGESTION_RATING))
				.param("comment", SUGGESTION_COMMENT)
				.sessionAttr("user", user))
				.andExpect(redirectedUrl("/insomniaclub/suggestion/" + suggestion.getId()));

		List<SuggestionRating> srs = srDao.findAll();
		found = false;
		for (SuggestionRating s : srs) {
			if (s.getUser().equals(user) && s.getSuggestion().equals(suggestion) &&
					s.getRating() == SUGGESTION_RATING) {
				found = true;
				break;
			}
		}
		assertTrue(found);

		List<SuggestionComment> scs = scDao.findAll();
		found = false;
		for (SuggestionComment s : scs) {
			if (s.getUser().equals(user) && s.getSuggestion().equals(suggestion) &&
					s.getComment().equals(SUGGESTION_COMMENT)) {
				found = true;
				break;
			}
		}
		assertTrue(found);
	}

}
