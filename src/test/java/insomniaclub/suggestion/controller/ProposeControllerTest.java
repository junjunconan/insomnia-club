package insomniaclub.suggestion.controller;

import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import insomniaclub.ControllerTestBase;
import insomniaclub.suggestion.model.Suggestion;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class ProposeControllerTest extends ControllerTestBase {

	@Before
	public void doSignup() throws Exception {
		signup();
	}

	@Test
	public void testPageRedirection() throws Exception {
		mockMvc.perform(get("/insomniaclub/propose"))
			.andExpect(redirectedUrl("/insomniaclub"));

		mockMvc.perform(get("/insomniaclub/propose").sessionAttr("user", user))
			.andExpect(view().name("propose"));
	}

	@Test
	public void testProposeAction() throws Exception {
		mockMvc.perform(post("/insomniaclub/propose"))
				.andExpect(redirectedUrl("/insomniaclub"));

		mockMvc.perform(post("/insomniaclub/propose")
				.param("title", SUGGESTION_TITLE)
				.param("description", SUGGESTION_DES)
				.sessionAttr("user", user))
				.andExpect(redirectedUrl("/insomniaclub"));

		List<Suggestion> retrieved = suggestionDao.findAll();
		boolean found = false;
		for (Suggestion s : retrieved) {
			if (s.getUser().equals(user) &&
				s.getTitle().equals(SUGGESTION_TITLE) &&
				s.getDescription().equals(SUGGESTION_DES)) {
				found = true;
				break;
			}
		}
		assertTrue(found);
	}

}
