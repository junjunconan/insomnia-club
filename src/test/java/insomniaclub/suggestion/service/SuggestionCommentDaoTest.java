package insomniaclub.suggestion.service;

import static org.junit.Assert.assertTrue;
import insomniaclub.TestBase;
import insomniaclub.authentication.model.User;
import insomniaclub.suggestion.model.Suggestion;
import insomniaclub.suggestion.model.SuggestionComment;

import java.util.List;

import org.junit.Test;

public class SuggestionCommentDaoTest extends TestBase {

	@Test
	public void testDatabasePersistency() throws Exception {
		User user = new User(EMAIL);
		user.setPassword(PASSWORD);
		user.setFirstName(FIRST_NAME);
		user.setLastName(LAST_NAME);
		udao.saveUser(user);

		Suggestion suggestion = new Suggestion(SUGGESTION_TITLE, SUGGESTION_DES, user);
		suggestionDao.saveSuggestion(suggestion);

		SuggestionComment suggestionComment = new SuggestionComment(SUGGESTION_COMMENT,
												user, suggestion);
		scDao.saveSuggestionComment(suggestionComment);
		List<SuggestionComment> scs = scDao.findAll();
		boolean found = false;
		for (SuggestionComment s : scs) {
			if (s.getUser().equals(user) && s.getSuggestion().equals(suggestion)) {
				found = true;
				break;
			}
		}
		assertTrue(found);
	}

}
