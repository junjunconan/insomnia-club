package insomniaclub.suggestion.service;

import static org.junit.Assert.assertTrue;
import insomniaclub.TestBase;
import insomniaclub.authentication.model.User;
import insomniaclub.suggestion.model.Suggestion;

import java.util.List;

import org.junit.Test;

public class SuggestionDaoTest extends TestBase {

	@Test
	public void testDatabasePersistency() throws Exception {
		User user = new User(EMAIL);
		user.setPassword(PASSWORD);
		user.setFirstName(FIRST_NAME);
		user.setLastName(LAST_NAME);
		udao.saveUser(user);

		Suggestion suggestion = new Suggestion(SUGGESTION_TITLE, SUGGESTION_DES, user);
		suggestionDao.saveSuggestion(suggestion);

		List<Suggestion> retrieved = suggestionDao.findAll();
		boolean found = false;
		for (Suggestion s : retrieved) {
			if (s.getUser().equals(user) &&
				s.getTitle().equals(SUGGESTION_TITLE) &&
				s.getDescription().equals(SUGGESTION_DES)) {
				found = true;
				break;
			}
		}
		assertTrue(found);
	}

}
