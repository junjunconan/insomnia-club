package insomniaclub.music.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import insomniaclub.ControllerTestBase;

import org.junit.Before;
import org.junit.Test;

public class MusicSearchControllerTest extends ControllerTestBase{
	@Before
	public void doSignup() throws Exception {
		signup();
	}
	
	@Test
	public void testPage() throws Exception{
		mockMvc.perform(get("/insomniaclub/music"))
			.andExpect(redirectedUrl("/insomniaclub"));
		
		mockMvc.perform(get("/insomniaclub/music").sessionAttr("user", user))
		.andExpect(view().name("musiclist"));
		}
}
