package insomniaclub.music.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.fileUpload;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import insomniaclub.ControllerTestBase;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.mock.web.MockMultipartFile;

public class AddMusicControllerTest extends ControllerTestBase{
	@Before
	public void doSignup() throws Exception {
		signup();
	}

	@Test
	public void testPageRedirction() throws Exception{
		mockMvc.perform(get("/insomniaclub/addmusic"))
			.andExpect(redirectedUrl("/insomniaclub"));

		mockMvc.perform(get("/insomniaclub/addmusic").sessionAttr("user", user))
		.andExpect(view().name("musicinfo"));
		}

	@Test
	public void testAddMusic() throws Exception{
		InputStream instream = new FileInputStream(new File("test.txt"));
		MockMultipartFile testFile = new MockMultipartFile(
				"file", "test.txt", "text/plain", instream);
		mockMvc.perform(fileUpload("/insomniaclub/addmusic")
				.file(testFile)
				.param("musicName", MUSIC_NAME)
				.param("musicType", MUSIC_TYPE)
				.param("singer", MUSIC_SINGER)
				.param("keyword1", MUSIC_KEYWORD1)
				.param("keyword2", MUSIC_KEYWORD2)
				.param("keyword3", MUSIC_KEYWORD3)
				.sessionAttr("user", user))
				.andExpect(redirectedUrl("/insomniaclub/music"));
	}

	@After
	public void deleteTestFile() throws Exception{
		File testFile = new File("music", "test.txt");
		if (testFile.isFile())
			testFile.delete();
	}

}
