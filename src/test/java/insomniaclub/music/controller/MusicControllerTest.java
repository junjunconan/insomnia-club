package insomniaclub.music.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.Before;
import org.junit.Test;

import insomniaclub.ControllerTestBase;

public class MusicControllerTest extends ControllerTestBase{
	@Before
	public void doSignup() throws Exception {
		signup();
	}
	
	@Test
	public void testPageRedirction() throws Exception{
		mockMvc.perform(get("/insomniaclub/music"))
			.andExpect(redirectedUrl("/insomniaclub"));
		
		mockMvc.perform(get("/insomniaclub/music").sessionAttr("user", user))
		.andExpect(view().name("musiclist"));
		}
	
	
}
