package insomniaclub.music.model;

import static org.junit.Assert.*;
import insomniaclub.TestBase;
import insomniaclub.timetable.model.TimeTable;

import org.junit.Test;

public class MusicTest extends TestBase {
	
	@Test
	public void testModel() throws Exception {
		
		@SuppressWarnings("unused")
		TimeTable timetable = new TimeTable();
		Music music = new Music();

		
		music.setMusicname(MUSIC_NAME);
		music.setMusictype(MUSIC_TYPE);
		music.setSinger(MUSIC_SINGER);
		music.setKeyword1(MUSIC_KEYWORD1);
		music.setKeyword2(MUSIC_KEYWORD2);
		music.setKeyword3(MUSIC_KEYWORD3);
		assertEquals(MUSIC_NAME, music.getMusicname());
		assertEquals(MUSIC_TYPE, music.getMusictype());
		assertEquals(MUSIC_SINGER, music.getSinger());
		assertEquals(MUSIC_KEYWORD1, music.getKeyword1());
		assertEquals(MUSIC_KEYWORD2, music.getKeyword2());
		assertEquals(MUSIC_KEYWORD3, music.getKeyword3());
		
		
		
	}

}
