package insomniaclub.music.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import insomniaclub.TestBase;
import insomniaclub.authentication.model.User;
import insomniaclub.music.model.Music;

import java.util.List;

import org.junit.Test;

public class MusicDaoTest extends TestBase {

	@Test
	public void testDatabasePersistency() throws Exception {
		User user = new User(EMAIL);
		user.setPassword(PASSWORD);
		user.setFirstName(FIRST_NAME);
		user.setLastName(LAST_NAME);
		udao.saveUser(user);

		Music music = new Music();
		music.setMusicname(MUSIC_NAME);
		music.setUser(user);
		musicDao.saveMusic(music);

		List<Music> retrieved = musicDao.findAll();
		boolean found = false;
		for (Music rmusic : retrieved) {
			if (rmusic.getUser().equals(user) && rmusic.getMusicname().equals(MUSIC_NAME)) {
				found = true;
				break;
			}
		}
		assertTrue(found);
	}

}
