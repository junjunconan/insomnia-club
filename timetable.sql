use insomnia;

drop table if exists timetable;
drop table if exists surveyform;
drop table if exists eventcheck;

CREATE table timetable(
	id 		int(11) not null AUTO_INCREMENT primary key,
	email 		varchar(255) not null,
	eventname 	varchar(255),
	eventtime 	TIME,
	foreign key(email) references user(email) on delete cascade on update cascade
);

create table surveyform (
  	id		int(100) not null  AUTO_INCREMENT primary key,
  	email		varchar(255),  		
  	job		varchar(100),
 	gtime           TIME,
  	btime           TIME,
  	typemusic       varchar(100),
  	aexercise	varchar(2),
  	aemail		varchar(2),
  	foreign key (email)	references user(email) on delete cascade on update cascade
);

CREATE table eventcheck(
	id		int(255) not null AUTO_INCREMENT primary key,
	email 		varchar(255) not null,
	event 		varchar(255),
	eventdate 	DATETIME,
	foreign key (email) references user(email) on delete cascade on update cascade
);
