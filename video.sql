create database if not exists insomnia;

use insomnia;

drop table if exists video;
drop table if exists videocomment;
drop table if exists videorate;
drop table if exists videotag;

create table video (
  videoid           int(50) not null primary key,
  videotitle        varchar(500) not null,
  category        varchar(500) not null,
  description           varchar(10000),
  videopath          varchar(255) not null
);
 insert into insomnia.video(videoid, videotitle,category,description,videopath) values("1", "what is insomnia","intro","Professional is telling you","https://www.youtube.com/embed/_tXFhGRqggc");
 insert into insomnia.video(videoid, videotitle,category,description,videopath) values("2", "What Makes Insomniac Brains Different?","intro","Insomnia isn't fun. It can have some pretty harsh physical and psychological side effects. Is insomnia actually changing your brain? Anthony breaks down some of the causes of insomnia, and explains what exactly it's doing to the brain.","https://www.youtube.com/embed/sWLOSwKXXOU?feature=player_detailpage");

 insert into insomnia.video(videoid, videotitle,category,description,videopath) values("3", "Bedtime Yoga","exe","Here are a few bedtime Yoga poses you can do, while in your bed to relax the body and mind and to have a restful sleep. This practice will be very beneficial after a stressful day or as a relaxation after a Yoga workout. For people who suffer from insomnia I would recommend to try out this practice.","https://www.youtube.com/embed/CgcW_4vssvM");
insert into insomnia.video(videoid, videotitle,category,description,videopath) values("4", "3 Exercises to Overcome Insomnia, Stop Thinking so Much, and Get a Good Night's Sleep","exe","When you have trouble sleeping you can try to use your thoughts to calm your mind but that just seems to add to your sleep anxiety. Instead, use these simple exercise and use you body to calm you thoughts, stop thinking so much, get out of your head, unscramble and balance your body's energies and get a good nights sleep.","https://www.youtube.com/embed/ZZRV-gSWSCc");

 insert into insomnia.video(videoid, videotitle,category,description,videopath) values("5", "Insomnia Cure","overcome","Insomnia Cure: Hypnosis May Help Overcome Insomnia","https://www.youtube.com/embed/CROwrwVL6Oo");
insert into insomnia.video(videoid, videotitle,category,description,videopath) values("6", "How to get a Good Night Sleep & Overcome Insomnia - #7 of Self-care NOT Health-care Series","overcome","The most common cause of sleeplessness is mental tension about by anxiety, worries, overwork and over excitement. Suppressed feelings of resentment, anger and bitterness may also cause insomnia. Constipation, overeating at night, excessive intake of tea or coffee, smoking and going to bed hungry are among other causes. Even worrying about falling asleep can be enough to keep a person awake.","https://www.youtube.com/embed/4z_22IH5z88");


create table videocomment (
  commentid     int(50) not null primary key auto_increment,
  comment       varchar(255) not null,
  email			     varchar(255),
  video_id              int(50),
   foreign key (email) 	references 		user(email) on delete cascade on update cascade,
    foreign key (video_id) references 	video(videoid) on delete cascade on update cascade
);

create table videorate (
  videorateid           int(50) not null primary key auto_increment,
  videorate             int(50) not null,
  email		            varchar(255),
  video_id              int(50),
  foreign key (email) 	references 		user(email) on delete cascade on update cascade,
   foreign key (video_id) references 	video(videoid) on delete cascade on update cascade
);

create table videotag (
  videotagid           int(50) not null primary key,
  videotagname        varchar(50) not null
);

/*insert into insomnia.videotag (videotagid, videotagname) values("1", "exercise");
select *
from videotag;*/