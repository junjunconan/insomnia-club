create database if not exists insomnia;

use insomnia;

drop table if exists user;

create table user (
  email			varchar(255) not null primary key,
  password		varchar(50),
  first_name	varchar(100) not null,
  last_name		varchar(100) not null,
  age			int(11),
  gender		varchar(2),
  date			timestamp
);